<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160307135508 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE auth_user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(255) NOT NULL, username_canonical VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, email_canonical VARCHAR(255) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, locked TINYINT(1) NOT NULL, expired TINYINT(1) NOT NULL, expires_at DATETIME DEFAULT NULL, confirmation_token VARCHAR(255) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', credentials_expired TINYINT(1) NOT NULL, credentials_expire_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, date_of_birth DATETIME DEFAULT NULL, firstname VARCHAR(64) DEFAULT NULL, lastname VARCHAR(64) DEFAULT NULL, website VARCHAR(64) DEFAULT NULL, biography VARCHAR(1000) DEFAULT NULL, gender VARCHAR(1) DEFAULT NULL, locale VARCHAR(8) DEFAULT NULL, timezone VARCHAR(64) DEFAULT NULL, phone VARCHAR(64) DEFAULT NULL, facebook_uid VARCHAR(255) DEFAULT NULL, facebook_name VARCHAR(255) DEFAULT NULL, facebook_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', twitter_uid VARCHAR(255) DEFAULT NULL, twitter_name VARCHAR(255) DEFAULT NULL, twitter_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', gplus_uid VARCHAR(255) DEFAULT NULL, gplus_name VARCHAR(255) DEFAULT NULL, gplus_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', token VARCHAR(255) DEFAULT NULL, two_step_code VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_A3B536FD92FC23A8 (username_canonical), UNIQUE INDEX UNIQ_A3B536FDA0D96FBF (email_canonical), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_user_group (user_id INT NOT NULL, group_id INT NOT NULL, INDEX IDX_D91914E1A76ED395 (user_id), INDEX IDX_D91914E1FE54D947 (group_id), PRIMARY KEY(user_id, group_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE auth_group (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', UNIQUE INDEX UNIQ_A1E215B05E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE auth_client (id INT AUTO_INCREMENT NOT NULL, random_id VARCHAR(255) NOT NULL, redirect_uris LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', secret VARCHAR(255) NOT NULL, allowed_grant_types LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE auth_access_token (id INT AUTO_INCREMENT NOT NULL, client_id INT NOT NULL, user_id INT DEFAULT NULL, token VARCHAR(255) NOT NULL, expires_at INT DEFAULT NULL, scope VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_48BAD46C5F37A13B (token), INDEX IDX_48BAD46C19EB6921 (client_id), INDEX IDX_48BAD46CA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE auth_refresh_token (id INT AUTO_INCREMENT NOT NULL, client_id INT NOT NULL, user_id INT DEFAULT NULL, token VARCHAR(255) NOT NULL, expires_at INT DEFAULT NULL, scope VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_C0DCFD855F37A13B (token), INDEX IDX_C0DCFD8519EB6921 (client_id), INDEX IDX_C0DCFD85A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE auth_authcode (id INT AUTO_INCREMENT NOT NULL, client_id INT NOT NULL, user_id INT DEFAULT NULL, token VARCHAR(255) NOT NULL, redirect_uri LONGTEXT NOT NULL, expires_at INT DEFAULT NULL, scope VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_8C699B995F37A13B (token), INDEX IDX_8C699B9919EB6921 (client_id), INDEX IDX_8C699B99A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE business_company (id INT AUTO_INCREMENT NOT NULL, owner INT DEFAULT NULL, active TINYINT(1) NOT NULL, position INT NOT NULL, name VARCHAR(255) DEFAULT NULL, logo_text VARCHAR(255) DEFAULT NULL, pitch LONGTEXT DEFAULT NULL, email VARCHAR(80) NOT NULL, tel VARCHAR(80) NOT NULL, fax VARCHAR(80) DEFAULT NULL, vat VARCHAR(80) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_128CB202CF60E67C (owner), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE business_job (id INT AUTO_INCREMENT NOT NULL, department_id INT DEFAULT NULL, category_id INT DEFAULT NULL, company_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, pitch VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, requirements LONGTEXT DEFAULT NULL, offer LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_65ABD188AE80F5DF (department_id), INDEX IDX_65ABD18812469DE2 (category_id), INDEX IDX_65ABD188979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE job_tag (job_id INT NOT NULL, tag_id INT NOT NULL, INDEX IDX_C75C2412BE04EA9 (job_id), INDEX IDX_C75C2412BAD26311 (tag_id), PRIMARY KEY(job_id, tag_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE business_department (id INT AUTO_INCREMENT NOT NULL, company_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, active TINYINT(1) NOT NULL, position INT NOT NULL, pitch VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_DBEE921979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE classification_category (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, position INT NOT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, active TINYINT(1) NOT NULL, description LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_83DA7A91727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE classification_tag (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, active TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE geo_address (id INT AUTO_INCREMENT NOT NULL, company_id INT DEFAULT NULL, department_id INT DEFAULT NULL, city_id INT DEFAULT NULL, country_id INT DEFAULT NULL, province_id INT DEFAULT NULL, marker_id INT DEFAULT NULL, street VARCHAR(255) NOT NULL, nr VARCHAR(40) NOT NULL, postcode VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_BA58E283979B1AD6 (company_id), UNIQUE INDEX UNIQ_BA58E283AE80F5DF (department_id), INDEX IDX_BA58E2838BAC62AF (city_id), INDEX IDX_BA58E283F92F3E70 (country_id), INDEX IDX_BA58E283E946114A (province_id), UNIQUE INDEX UNIQ_BA58E283474460EB (marker_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_address (address_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_5543718BF5B7AF75 (address_id), INDEX IDX_5543718BA76ED395 (user_id), PRIMARY KEY(address_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE geo_city (id INT AUTO_INCREMENT NOT NULL, country_id INT DEFAULT NULL, province_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_297C2D34F92F3E70 (country_id), INDEX IDX_297C2D34E946114A (province_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE geo_country (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, iso_name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE geo_province (id INT AUTO_INCREMENT NOT NULL, country_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, iso_name VARCHAR(255) DEFAULT NULL, INDEX IDX_A463A3AAF92F3E70 (country_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE geo_map (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, center_lat DOUBLE PRECISION NOT NULL, center_lng DOUBLE PRECISION NOT NULL, auto_zoom TINYINT(1) NOT NULL, zoom_level INT NOT NULL, style VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE geo_marker (id INT AUTO_INCREMENT NOT NULL, map_id INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, lat DOUBLE PRECISION NOT NULL, lng DOUBLE PRECISION NOT NULL, icon VARCHAR(255) DEFAULT NULL, image VARCHAR(255) DEFAULT NULL, animation VARCHAR(40) DEFAULT NULL, options LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_291E198053C55F64 (map_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_notification_message (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) NOT NULL, body LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', state INT NOT NULL, restart_count INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, started_at DATETIME DEFAULT NULL, completed_at DATETIME DEFAULT NULL, INDEX state (state), INDEX createdAt (created_at), INDEX idx_state (state), INDEX idx_created_at (created_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ext_translations (id INT AUTO_INCREMENT NOT NULL, locale VARCHAR(8) NOT NULL, object_class VARCHAR(255) NOT NULL, field VARCHAR(32) NOT NULL, foreign_key VARCHAR(64) NOT NULL, content LONGTEXT DEFAULT NULL, INDEX translations_lookup_idx (locale, object_class, foreign_key), UNIQUE INDEX lookup_unique_idx (locale, object_class, field, foreign_key), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ext_log_entries (id INT AUTO_INCREMENT NOT NULL, action VARCHAR(8) NOT NULL, logged_at DATETIME NOT NULL, object_id VARCHAR(64) DEFAULT NULL, object_class VARCHAR(255) NOT NULL, version INT NOT NULL, data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', username VARCHAR(255) DEFAULT NULL, INDEX log_class_lookup_idx (object_class), INDEX log_date_lookup_idx (logged_at), INDEX log_user_lookup_idx (username), INDEX log_version_lookup_idx (object_id, object_class, version), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE acl_classes (id INT UNSIGNED AUTO_INCREMENT NOT NULL, class_type VARCHAR(200) NOT NULL, UNIQUE INDEX UNIQ_69DD750638A36066 (class_type), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE acl_security_identities (id INT UNSIGNED AUTO_INCREMENT NOT NULL, identifier VARCHAR(200) NOT NULL, username TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_8835EE78772E836AF85E0677 (identifier, username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE acl_object_identities (id INT UNSIGNED AUTO_INCREMENT NOT NULL, parent_object_identity_id INT UNSIGNED DEFAULT NULL, class_id INT UNSIGNED NOT NULL, object_identifier VARCHAR(100) NOT NULL, entries_inheriting TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_9407E5494B12AD6EA000B10 (object_identifier, class_id), INDEX IDX_9407E54977FA751A (parent_object_identity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE acl_object_identity_ancestors (object_identity_id INT UNSIGNED NOT NULL, ancestor_id INT UNSIGNED NOT NULL, INDEX IDX_825DE2993D9AB4A6 (object_identity_id), INDEX IDX_825DE299C671CEA1 (ancestor_id), PRIMARY KEY(object_identity_id, ancestor_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE acl_entries (id INT UNSIGNED AUTO_INCREMENT NOT NULL, class_id INT UNSIGNED NOT NULL, object_identity_id INT UNSIGNED DEFAULT NULL, security_identity_id INT UNSIGNED NOT NULL, field_name VARCHAR(50) DEFAULT NULL, ace_order SMALLINT UNSIGNED NOT NULL, mask INT NOT NULL, granting TINYINT(1) NOT NULL, granting_strategy VARCHAR(30) NOT NULL, audit_success TINYINT(1) NOT NULL, audit_failure TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_46C8B806EA000B103D9AB4A64DEF17BCE4289BF4 (class_id, object_identity_id, field_name, ace_order), INDEX IDX_46C8B806EA000B103D9AB4A6DF9183C9 (class_id, object_identity_id, security_identity_id), INDEX IDX_46C8B806EA000B10 (class_id), INDEX IDX_46C8B8063D9AB4A6 (object_identity_id), INDEX IDX_46C8B806DF9183C9 (security_identity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE app_user_group ADD CONSTRAINT FK_D91914E1A76ED395 FOREIGN KEY (user_id) REFERENCES auth_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE app_user_group ADD CONSTRAINT FK_D91914E1FE54D947 FOREIGN KEY (group_id) REFERENCES auth_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE auth_access_token ADD CONSTRAINT FK_48BAD46C19EB6921 FOREIGN KEY (client_id) REFERENCES auth_client (id)');
        $this->addSql('ALTER TABLE auth_access_token ADD CONSTRAINT FK_48BAD46CA76ED395 FOREIGN KEY (user_id) REFERENCES auth_user (id)');
        $this->addSql('ALTER TABLE auth_refresh_token ADD CONSTRAINT FK_C0DCFD8519EB6921 FOREIGN KEY (client_id) REFERENCES auth_client (id)');
        $this->addSql('ALTER TABLE auth_refresh_token ADD CONSTRAINT FK_C0DCFD85A76ED395 FOREIGN KEY (user_id) REFERENCES auth_user (id)');
        $this->addSql('ALTER TABLE auth_authcode ADD CONSTRAINT FK_8C699B9919EB6921 FOREIGN KEY (client_id) REFERENCES auth_client (id)');
        $this->addSql('ALTER TABLE auth_authcode ADD CONSTRAINT FK_8C699B99A76ED395 FOREIGN KEY (user_id) REFERENCES auth_user (id)');
        $this->addSql('ALTER TABLE business_company ADD CONSTRAINT FK_128CB202CF60E67C FOREIGN KEY (owner) REFERENCES auth_user (id)');
        $this->addSql('ALTER TABLE business_job ADD CONSTRAINT FK_65ABD188AE80F5DF FOREIGN KEY (department_id) REFERENCES business_department (id)');
        $this->addSql('ALTER TABLE business_job ADD CONSTRAINT FK_65ABD18812469DE2 FOREIGN KEY (category_id) REFERENCES classification_category (id)');
        $this->addSql('ALTER TABLE business_job ADD CONSTRAINT FK_65ABD188979B1AD6 FOREIGN KEY (company_id) REFERENCES business_company (id)');
        $this->addSql('ALTER TABLE job_tag ADD CONSTRAINT FK_C75C2412BE04EA9 FOREIGN KEY (job_id) REFERENCES business_job (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE job_tag ADD CONSTRAINT FK_C75C2412BAD26311 FOREIGN KEY (tag_id) REFERENCES classification_tag (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE business_department ADD CONSTRAINT FK_DBEE921979B1AD6 FOREIGN KEY (company_id) REFERENCES business_company (id)');
        $this->addSql('ALTER TABLE classification_category ADD CONSTRAINT FK_83DA7A91727ACA70 FOREIGN KEY (parent_id) REFERENCES classification_category (id)');
        $this->addSql('ALTER TABLE geo_address ADD CONSTRAINT FK_BA58E283979B1AD6 FOREIGN KEY (company_id) REFERENCES business_company (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE geo_address ADD CONSTRAINT FK_BA58E283AE80F5DF FOREIGN KEY (department_id) REFERENCES business_department (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE geo_address ADD CONSTRAINT FK_BA58E2838BAC62AF FOREIGN KEY (city_id) REFERENCES geo_city (id)');
        $this->addSql('ALTER TABLE geo_address ADD CONSTRAINT FK_BA58E283F92F3E70 FOREIGN KEY (country_id) REFERENCES geo_country (id)');
        $this->addSql('ALTER TABLE geo_address ADD CONSTRAINT FK_BA58E283E946114A FOREIGN KEY (province_id) REFERENCES geo_province (id)');
        $this->addSql('ALTER TABLE geo_address ADD CONSTRAINT FK_BA58E283474460EB FOREIGN KEY (marker_id) REFERENCES geo_marker (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE user_address ADD CONSTRAINT FK_5543718BF5B7AF75 FOREIGN KEY (address_id) REFERENCES geo_address (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_address ADD CONSTRAINT FK_5543718BA76ED395 FOREIGN KEY (user_id) REFERENCES auth_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE geo_city ADD CONSTRAINT FK_297C2D34F92F3E70 FOREIGN KEY (country_id) REFERENCES geo_country (id)');
        $this->addSql('ALTER TABLE geo_city ADD CONSTRAINT FK_297C2D34E946114A FOREIGN KEY (province_id) REFERENCES geo_province (id)');
        $this->addSql('ALTER TABLE geo_province ADD CONSTRAINT FK_A463A3AAF92F3E70 FOREIGN KEY (country_id) REFERENCES geo_country (id)');
        $this->addSql('ALTER TABLE geo_marker ADD CONSTRAINT FK_291E198053C55F64 FOREIGN KEY (map_id) REFERENCES geo_map (id)');
        $this->addSql('ALTER TABLE acl_object_identities ADD CONSTRAINT FK_9407E54977FA751A FOREIGN KEY (parent_object_identity_id) REFERENCES acl_object_identities (id)');
        $this->addSql('ALTER TABLE acl_object_identity_ancestors ADD CONSTRAINT FK_825DE2993D9AB4A6 FOREIGN KEY (object_identity_id) REFERENCES acl_object_identities (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE acl_object_identity_ancestors ADD CONSTRAINT FK_825DE299C671CEA1 FOREIGN KEY (ancestor_id) REFERENCES acl_object_identities (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE acl_entries ADD CONSTRAINT FK_46C8B806EA000B10 FOREIGN KEY (class_id) REFERENCES acl_classes (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE acl_entries ADD CONSTRAINT FK_46C8B8063D9AB4A6 FOREIGN KEY (object_identity_id) REFERENCES acl_object_identities (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE acl_entries ADD CONSTRAINT FK_46C8B806DF9183C9 FOREIGN KEY (security_identity_id) REFERENCES acl_security_identities (id) ON UPDATE CASCADE ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE app_user_group DROP FOREIGN KEY FK_D91914E1A76ED395');
        $this->addSql('ALTER TABLE auth_access_token DROP FOREIGN KEY FK_48BAD46CA76ED395');
        $this->addSql('ALTER TABLE auth_refresh_token DROP FOREIGN KEY FK_C0DCFD85A76ED395');
        $this->addSql('ALTER TABLE auth_authcode DROP FOREIGN KEY FK_8C699B99A76ED395');
        $this->addSql('ALTER TABLE business_company DROP FOREIGN KEY FK_128CB202CF60E67C');
        $this->addSql('ALTER TABLE user_address DROP FOREIGN KEY FK_5543718BA76ED395');
        $this->addSql('ALTER TABLE app_user_group DROP FOREIGN KEY FK_D91914E1FE54D947');
        $this->addSql('ALTER TABLE auth_access_token DROP FOREIGN KEY FK_48BAD46C19EB6921');
        $this->addSql('ALTER TABLE auth_refresh_token DROP FOREIGN KEY FK_C0DCFD8519EB6921');
        $this->addSql('ALTER TABLE auth_authcode DROP FOREIGN KEY FK_8C699B9919EB6921');
        $this->addSql('ALTER TABLE business_job DROP FOREIGN KEY FK_65ABD188979B1AD6');
        $this->addSql('ALTER TABLE business_department DROP FOREIGN KEY FK_DBEE921979B1AD6');
        $this->addSql('ALTER TABLE geo_address DROP FOREIGN KEY FK_BA58E283979B1AD6');
        $this->addSql('ALTER TABLE job_tag DROP FOREIGN KEY FK_C75C2412BE04EA9');
        $this->addSql('ALTER TABLE business_job DROP FOREIGN KEY FK_65ABD188AE80F5DF');
        $this->addSql('ALTER TABLE geo_address DROP FOREIGN KEY FK_BA58E283AE80F5DF');
        $this->addSql('ALTER TABLE business_job DROP FOREIGN KEY FK_65ABD18812469DE2');
        $this->addSql('ALTER TABLE classification_category DROP FOREIGN KEY FK_83DA7A91727ACA70');
        $this->addSql('ALTER TABLE job_tag DROP FOREIGN KEY FK_C75C2412BAD26311');
        $this->addSql('ALTER TABLE user_address DROP FOREIGN KEY FK_5543718BF5B7AF75');
        $this->addSql('ALTER TABLE geo_address DROP FOREIGN KEY FK_BA58E2838BAC62AF');
        $this->addSql('ALTER TABLE geo_address DROP FOREIGN KEY FK_BA58E283F92F3E70');
        $this->addSql('ALTER TABLE geo_city DROP FOREIGN KEY FK_297C2D34F92F3E70');
        $this->addSql('ALTER TABLE geo_province DROP FOREIGN KEY FK_A463A3AAF92F3E70');
        $this->addSql('ALTER TABLE geo_address DROP FOREIGN KEY FK_BA58E283E946114A');
        $this->addSql('ALTER TABLE geo_city DROP FOREIGN KEY FK_297C2D34E946114A');
        $this->addSql('ALTER TABLE geo_marker DROP FOREIGN KEY FK_291E198053C55F64');
        $this->addSql('ALTER TABLE geo_address DROP FOREIGN KEY FK_BA58E283474460EB');
        $this->addSql('ALTER TABLE acl_entries DROP FOREIGN KEY FK_46C8B806EA000B10');
        $this->addSql('ALTER TABLE acl_entries DROP FOREIGN KEY FK_46C8B806DF9183C9');
        $this->addSql('ALTER TABLE acl_object_identities DROP FOREIGN KEY FK_9407E54977FA751A');
        $this->addSql('ALTER TABLE acl_object_identity_ancestors DROP FOREIGN KEY FK_825DE2993D9AB4A6');
        $this->addSql('ALTER TABLE acl_object_identity_ancestors DROP FOREIGN KEY FK_825DE299C671CEA1');
        $this->addSql('ALTER TABLE acl_entries DROP FOREIGN KEY FK_46C8B8063D9AB4A6');
        $this->addSql('DROP TABLE auth_user');
        $this->addSql('DROP TABLE app_user_group');
        $this->addSql('DROP TABLE auth_group');
        $this->addSql('DROP TABLE auth_client');
        $this->addSql('DROP TABLE auth_access_token');
        $this->addSql('DROP TABLE auth_refresh_token');
        $this->addSql('DROP TABLE auth_authcode');
        $this->addSql('DROP TABLE business_company');
        $this->addSql('DROP TABLE business_job');
        $this->addSql('DROP TABLE job_tag');
        $this->addSql('DROP TABLE business_department');
        $this->addSql('DROP TABLE classification_category');
        $this->addSql('DROP TABLE classification_tag');
        $this->addSql('DROP TABLE geo_address');
        $this->addSql('DROP TABLE user_address');
        $this->addSql('DROP TABLE geo_city');
        $this->addSql('DROP TABLE geo_country');
        $this->addSql('DROP TABLE geo_province');
        $this->addSql('DROP TABLE geo_map');
        $this->addSql('DROP TABLE geo_marker');
        $this->addSql('DROP TABLE app_notification_message');
        $this->addSql('DROP TABLE ext_translations');
        $this->addSql('DROP TABLE ext_log_entries');
        $this->addSql('DROP TABLE acl_classes');
        $this->addSql('DROP TABLE acl_security_identities');
        $this->addSql('DROP TABLE acl_object_identities');
        $this->addSql('DROP TABLE acl_object_identity_ancestors');
        $this->addSql('DROP TABLE acl_entries');
    }
}
