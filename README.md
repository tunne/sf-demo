# SF Demo
=========

A demo project using Symfony Framework.

## Instructions
Create a CRUD system create, read, update, delete.
What is the CRUD about: Jobs + location

###Technical:
- Framework Symfony2.
- Query: ORM Doctrine, ... do not use plain sql code
- Front: Twitter bootstrap
- API: items should be available via an REST API (oAuth2 secured).
- Dependancies: Composer

## Solution
### Requirements
- PHP
- MYSQL
- Git
- Composer

- Optional
    - Virtualbox
    - Vagrant
    - Ansible

### Used vendors / Bundles
- Symfony (Framework SE)
- Doctrine ORM
- JMS (Serialization)
- Friends of Symfony (Auth - user management && REST)
- Sonata (CRUD) 

### Installation
#### Clone the git repository.
```
git clone git@bitbucket.org:tunne/sf-demo.git <projectname>
```

#### Init and update git submodules.
```
cd <projectname>
git submodule init
git submodule update
```

#### Run composer install
```
composer install
```

#### Cache and log permisions
- Remove mud
```
rm -rf app/cache/*
rm -rf app/logs/*
```
- Set permissions according to the system
1. Using ACL on a system that supports chmod +a
```
HTTPDUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
sudo chmod +a "$HTTPDUSER allow delete,write,append,file_inherit,directory_inherit" app/cache app/logs
sudo chmod +a "`whoami` allow delete,write,append,file_inherit,directory_inherit" app/cache app/logs
```

2. Using ACL on a system that does not support chmod +a
```
HTTPDUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX app/cache app/logs
sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX app/cache app/logs
```

3. Without using ACL
```
umask(0002); // This will let the permissions be 0775
// or
umask(0000); // This will let the permissions be 0777

HTTPDUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX app/cache app/logs
sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX app/cache app/logs

HTTPDUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX cache
sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX cache
```

Clear the cache to be sure
```
php app/console cache:clear && php app/console cache:clear --env=prod
```

#### Run migrations
```
php app/console doctrine:migration:migrate
```

#### Add couttries, provinces, cities
Todo: Write fixture for this
```
mysql -u <user> -p <database> < db/country.sql
mysql -u <user> -p <database> < db/province.sql
mysql -u <user> -p <database> < db/city.sql

```

#### Add a user with proper roles
```
php app/console fos:user:create --super-admin
php app/console fos:user:promote (ROLE_SONATA_ADMIN)
```

#### Generate Client
- Client creation
```
php app/console app:auth-client:create --redirect-uri="http://demo-api.gillesdemeyer.be/" --grant-type="authorization_code" --grant-type="password" --grant-type="refresh_token" --grant-type="token" --grant-type="client_credentials"

```

Demo response:
``` 
Added a new client with public id 1_2apnef2facw0o4g4o04oogk0gkk8wcckcooog0kw044go4wsow, secret 4lshylupilmog84c0gwwoscwo4gksogookg4ks0wcs08s008o0
```

- Requests

Create Token:
A token should be returned when the client id and secret are correct.

```
http://<domain>/oauth/v2/token?client_id=<CLIENT_ID>&client_secret=<CLIENT_SECRET>&grant_type=client_credentials

```

Demo token response
```
{"access_token":"MTIwNmM2MDUxNjc0ZTBiNDk4MjE3ZmEwOWFmMGU1ZmVkMDllYzQ4ZGE0MmU1Yjk5OTgwZDk4NDNjMWIxYTljZQ","expires_in":3600,"token_type":"bearer","scope":"user"}

```
### Using the token
```
http://<domain>/api/address?limit=20&access_token=OWJlODBiNzNlZTNkZTcwZjcyZDkxYTM3NDBkYzc0MGI2YjZlOTJkYjQzNjdlMGIxYmE4ZGNhNjRmOTBiZTg2Mw

```


### Run application and test application.

## Backlog
- Application development
    - complete api
    - Add media (images, documents, ...) (use discriminator pattern)
    - Proper object interfaces
    - Check and test relations
    - Frontend application
- Development environment (complete vagrant box)
    - Configure ngix, php, mysql
    - Create virtual host
    - Install && Cofigure app
    - Create && Migrate database
- Testing
    - unit testing
    - functional testing
- CI environment
- Ansible deployment role
