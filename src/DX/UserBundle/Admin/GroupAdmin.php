<?php
/**
 * GroupAdmin.php.
 */

namespace DX\UserBundle\Admin;

use Sonata\UserBundle\Admin\Model\GroupAdmin as BaseGroupAdmin;

/**
 * Class GroupAdmin.
 */
class GroupAdmin extends BaseGroupAdmin
{
    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);
        $this->baseRouteName = 'useradmin_group';
        $this->baseRoutePattern = 'user/group';
    }
}
