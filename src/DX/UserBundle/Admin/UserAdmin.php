<?php
/**
 * UserAdmin.php.
 */

namespace DX\UserBundle\Admin;

use Sonata\UserBundle\Admin\Model\UserAdmin as BaseUserAdmin;

/**
 * Class UserAdmin.
 */
class UserAdmin extends BaseUserAdmin
{
    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);
        $this->baseRouteName = 'useradmin_user';
        $this->baseRoutePattern = 'user/user';
    }
}
