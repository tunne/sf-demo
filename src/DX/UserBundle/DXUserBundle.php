<?php
/**
 * This file is part of the project_dx project.
 *
 * (c) Gilles Demeyer g.demeyer@me.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DX\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * References :
 *   bundles : http://symfony.com/doc/current/book/bundles.html.
 *
 * @author Gilles Demeyer g.demeyer@me.com
 */
class DXUserBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'SonataUserBundle';
    }
}
