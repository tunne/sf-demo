<?php
/**
 * This file is part of the project_dx project.
 *
 * (c) Gilles Demeyer g.demeyer@me.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DX\AdminBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class DXAdminBundle.
 *
 * @author Gilles Demeyer g.demeyer@me.com
 */
class DXAdminBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'SonataAdminBundle';
    }
}
