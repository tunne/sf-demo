<?php
/**
 * MessageAdmin.php.
 */

namespace DX\NotificationBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\NotificationBundle\Admin\MessageAdmin as BaseAdmin;

/**
 * Class MessageAdmin.
 */
class MessageAdmin extends BaseAdmin
{
    /**
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     */
    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);
        $this->baseRouteName = 'notification_admin_message';
        $this->baseRoutePattern = 'notification/message';
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('type')
            ->add('createdAt')
            ->add('startedAt')
            ->add('completedAt')
            ->add('getStateName')
            ->add('body')
            ->add('restartCount')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('edit')
            ->remove('create')
            ->remove('history')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, array('route' => array('name' => 'show')))
            ->add('type')
            ->add('createdAt')
            ->add('startedAt')
            ->add('completedAt')
            ->add('getStateName')
            ->add('restartCount')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $class = $this->getClass();

        $datagridMapper
            ->add('type')
            ->add('state', null, array(), 'choice', array('choices' => $class::getStateList()))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBatchActions()
    {
        $actions = array();
        $actions['publish'] = array(
            'label'            => $this->trans($this->getLabelTranslatorStrategy()->getLabel('publish', 'batch', 'message')),
            'ask_confirmation' => false,
        );

        $actions['cancelled'] = array(
            'label'            => $this->trans($this->getLabelTranslatorStrategy()->getLabel('cancelled', 'batch', 'message')),
            'ask_confirmation' => false,
        );

        return $actions;
    }
}
