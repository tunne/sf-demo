<?php
/**
 * Configuration.php.
 */

namespace AppBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration.
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('app');

        $rootNode
            ->children()
                ->scalarNode('domain')
                ->end()
                ->scalarNode('api_domain')
                ->end()
                ->scalarNode('api_client')
                ->end()
                ->scalarNode('api_secret')
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
