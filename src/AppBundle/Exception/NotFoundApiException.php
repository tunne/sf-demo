<?php
/**
 * NotFoundApiException.php.
 */

namespace AppBundle\Exception;

use Exception;

/**
 * Class NotFoundApiException.
 */
class NotFoundApiException extends Exception
{
    public function __construct($message = "", $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
