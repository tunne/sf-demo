<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class DepartmentAdmin extends BaseAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('active')
            ->add('position')
            ->add('pitch')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name')
            ->add('active')
            ->add('position')
            ->add('company')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('position')
            ->add('active', 'checkbox', array(
                'required' => false,
            ))
            ->add('company', 'sonata_type_model_list', array(
                    'required' => false,
                    //'compound' => true,
                    'by_reference' => true,
                )
            )
            ->add('address', 'sonata_type_model_list', array(
                    'required' => false,
                    //'compound' => true,
                    'by_reference' => true,
                )
            )
            ->add('position')
            ->add('pitch', 'text', array(
                'required' => false,
            ))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('site')
            ->add('name')
            ->add('active')
            ->add('position')
            ->add('pitch')
        ;
    }
}
