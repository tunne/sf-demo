<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;

class BaseAdmin extends Admin
{
    /**
     * setBaseRoute.
     *
     * @param [type] $routeName    [description]
     * @param [type] $routePattern [description]
     */
    public function setBaseRoute($routeName, $routePattern)
    {
        $this->baseRouteName = $routeName;
        $this->baseRoutePattern = $routePattern;
    }
}
