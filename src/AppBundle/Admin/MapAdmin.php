<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class MapAdmin extends BaseAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('centerLat')
            ->add('centerLng')
            ->add('autoZoom')
            ->add('zoomLevel')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('name')
            ->add('centerLat')
            ->add('centerLng')
            ->add('autoZoom')
            ->add('zoomLevel')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('centerLat', 'text')
            ->add('centerLng', 'text')
            ->add('autoZoom', 'checkbox', array(
                'required' => false,
                'label' => 'Auto zoom',
            ))
            ->add('zoomLevel')
            //->add('style', 'map_style_choice')
            ->add('markers', 'sonata_type_collection',
                array(
                    'cascade_validation' => true,
                    'label'             => 'Markers',
                    'type_options' => array(
                        'delete' => true,
                    ),
                    'by_reference' => false,
                ), array(
                    'edit'              => 'inline',
                    'inline'            => 'table',
                    'sortable'          => 'position',
                )
            )
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('name')
            ->add('centerLat')
            ->add('centerLng')
            ->add('autoZoom')
            ->add('zoomLevel')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }
}
