<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class CompanyAdmin extends BaseAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('active')
            ->add('name')
            ->add('email')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('default')
            ->add('active')
            ->add('name')
            ->add('addresss')
            ->add('tel')
            ->add('email')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('owner', 'sonata_type_model_list',
                array(
                    'required' => false,
                    //'compound' => true,
                    'by_reference' => true,
                )
            )
            ->add('active', 'checkbox', array(
                'required' => false,
            ))
            ->add('position')
            ->add('name')
            ->add('email')
            ->add('tel')
            ->add('fax')
            //->add('logoText')
            ->add('pitch', null, array(
                'required' => false,
                'attr' => array(
                    'class' => 'ckeditor',
                )
            ))
            ->add('vat', null, array(
                'required' => false,
            ))
            ->add('address', 'sonata_type_model_list', array(
                'required' => false,
                'by_reference' => true,
            ))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('default')
            ->add('active')
            ->add('position')
            ->add('name')
            ->add('email')
            ->add('tel')
            ->add('fax')
            ->add('address')
            ->add('logoText')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }
}
