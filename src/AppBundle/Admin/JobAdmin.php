<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class JobAdmin extends BaseAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('name')
            ->add('company')
            ->add('department')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', 'text', array(
                'required' => true,
            ))
            ->add('company', 'sonata_type_model_list', array(
                    'required' => false,
                    //'compound' => true,
                    'by_reference' => true,
                )
            )
            ->add('company', 'sonata_type_model_list', array(
                    'required' => false,
                    //'compound' => true,
                    'by_reference' => true,
                )
            )
            ->add('category', 'sonata_type_model_list', array(
                'required' => false,
                'by_reference' => true,
            ))
            ->add('tags', 'sonata_type_model', array(
                'multiple' => true,
                'required' => false,
            ))
            ->add('pitch', 'text', array(
                'required' => false,
            ))
            ->add('type', 'business_job_type', array(
                'required' => true,
            ))
            ->add('description', 'textarea', array(
                'required' => false,
                'attr' => array(
                    'class' => 'ckeditor',
                ),
            ))
            ->add('requirements', 'textarea', array(
                'required' => false,
                'attr' => array(
                    'class' => 'ckeditor',
                ),
            ))
            ->add('offer', 'textarea', array(
                'required' => false,
                'attr' => array(
                    'class' => 'ckeditor',
                ),
            ))

        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('name')
        ;
    }
}
