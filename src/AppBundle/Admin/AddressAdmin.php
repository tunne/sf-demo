<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class AddressAdmin extends BaseAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('street')
            ->add('postcode')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('street')
            ->add('nr')
            ->add('postcode')
            ->add('city')
            ->add('province')
            ->add('country')
            ->add('createdAt')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('street')
            ->add('nr')
            ->add('postcode')
            ->add('city', 'sonata_type_model_autocomplete', array(
                'property' => 'name',
                'attr' => array(
                    'style' => 'width:100%;',
                ),
            ))
            ->add('province', 'sonata_type_model_autocomplete', array(
                'property' => 'name',
                'attr' => array(
                    'style' => 'width:100%;',
                ),
            ))
            ->add('country', 'sonata_type_model_autocomplete', array(
                'property' => 'name',
                'attr' => array(
                    'style' => 'width:100%;',
                ),
            ))
            ->add('marker', 'sonata_type_admin', array(
                'required' => false,
                ),
                array(
                    'edit' => 'inline',
            ))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('firstName')
            ->add('lastName')
            ->add('phoneNumber')
            ->add('street')
            ->add('postcode')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }
}
