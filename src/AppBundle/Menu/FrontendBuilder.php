<?php
namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Symfony\Component\Translation\TranslatorInterface;
use Model\Business\Repository\CompanyRepositoryInterface;
use Doctrine\Common\Persistence\ObjectRepository;

/**
 * Class RoomMenuBuilder
 * @package Academie\ThemeBundle\Menu
 */
class FrontendBuilder
{
    /**
     * @var FactoryInterface
     */
    private $factory;
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var CompanyRepositoryInterface
     */
    private $companyRepository;

    /**
     * @var ObjectRepository
     */
    private $categoryRepository;

    /**
     * @var ObjectRepository
     */
    private $tagRepository;

    /**
     * @param FactoryInterface $factory
     * @param RequestStack $requestStack
     * @param CompanyRepositoryInterface $companyRepository
     * @param ObjectRepository $categoryRepository
     * @param ObjectRepository $tagRepository
     */
    public function __construct(
        FactoryInterface $factory,
        RequestStack $requestStack,
        TranslatorInterface $translator,
        CompanyRepositoryInterface $companyRepository,
        ObjectRepository $categoryRepository,
        ObjectRepository $tagRepository
    ) {
        $this->factory = $factory;
        $this->requestStack = $requestStack;
        $this->translator = $translator;
        $this->companyRepository = $companyRepository;
        $this->categoryRepository = $categoryRepository;
        $this->tagRepository = $tagRepository;
    }


    public function createMainMenu()
    {
        $menu = $this->factory->createItem('root', array(
            'childrenAttributes' => array('class' => 'nav navbar-nav')
        ));

        $menu->addChild('home', array(
            'route' => 'app_default_index',
        ))->setLabel($this->translator->trans('app.menu.label.home'));
        $menu->addChild('category', array(
            'route' => 'app_category_index',
        ))->setLabel($this->translator->trans('app.menu.label.category'));
        $menu->addChild('company', array(
            'route' => 'app_company_index',
        ))->setLabel($this->translator->trans('app.menu.label.company'));

        return $menu;
    }



}