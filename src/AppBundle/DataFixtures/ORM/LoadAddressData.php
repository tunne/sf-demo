<?php
// src/AppBundle/DataFixtures/ORM/LoadUserData.php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Model\Geo\Repository\AddressRepository;
use Model\Geo\Address;

/**
 * Class LoadAddressData
 * @package AppBundle\DataFixtures\ORM
 */
class LoadAddressData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        try {
            /** @var AddressRepository $addressRepo */
            $addressRepo = $this->container->get('model_geo.repository.address');
            /** @var \Faker\Generator $faker */
            $faker = $this->container->get('faker.generator');

            $inc = 1;
            while ($inc < 100) {
                $companyRef = 'company-'.$inc;
                /** @var Address $address */
                $address = $addressRepo->createNew();
                $address
                    ->setStreet($faker->streetName)
                    ->setNr($faker->numberBetween(0,150))
                    ->setCity($faker->city)
                    ->setCountry()
                $inc++;
            }
            $manager->flush();

        } catch (\Exception $e) {
            $this->container->get('logger')->error($e->getMessage());
        }
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 3;
    }
}