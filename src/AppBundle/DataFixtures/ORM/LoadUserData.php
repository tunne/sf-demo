<?php
// src/AppBundle/DataFixtures/ORM/LoadUserData.php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LoadUserData
 * @package AppBundle\DataFixtures\ORM
 */
class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        try{
            /** @var \FOS\UserBundle\Model\UserManager $userManager */
            $userManager = $this->container->get('fos_user.user_manager');
            /** @var \Faker\Generator $faker */
            $faker = $this->container->get('faker.generator');

            $user = $userManager->createUser();
            $user
                ->setEmail('admin@sf-demo.local')
                ->setUsername('admin')
                ->setPlainPassword('admin')
                ->setLocked(false)
                ->setEnabled(true)
                ->setRoles(array('ROLE_SUPER_ADMIN', 'ROLE_SONATA_ADMIN', 'ROLE_ADMIN'))
            ;
            $userManager->updateUser($user);

            $inc = 1;
            while ($inc < 100) {
                $user = $userManager->createUser();
                $user
                    ->setEmail($faker->email)
                    ->setUsername($faker->userName)
                    ->setFirstName($faker->firstName)
                    ->setLastName($faker->lastName)
                    ->setPlainPassword($faker->password)
                    ->setLocked(false)
                    ->setEnabled(true)
                    ->setRoles(array('ROLE_USER'))
                ;
                $userManager->updateUser($user);
                $ref = 'user-'.$inc;
                $this->addReference($ref, $user);
                $inc++;
            }

            $manager->flush();
        } catch (\Exception $e) {
            $this->container->get('logger')->error($e->getMessage());
        }
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 1;
    }
}