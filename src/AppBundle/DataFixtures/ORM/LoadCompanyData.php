<?php
// src/AppBundle/DataFixtures/ORM/LoadUserData.php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Model\Business\Repository\CompanyRepositoryInterface;
use Model\Business\Company;

/**
 * Class LoadCompanyData
 * @package AppBundle\DataFixtures\ORM
 */
class LoadCompanyData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        try {
            /** @var CompanyRepositoryInterface $companyRepo */
            $companyRepo = $this->container->get('model_business.repository.company');
            /** @var \Faker\Generator $faker */
            $faker = $this->container->get('faker.generator');

            $inc = 1;
            while ($inc < 100) {
                $userRef = 'user-'.$inc;
                /** @var Company $company */
                $company = $companyRepo->createNew();
                $company
                    ->setName($faker->company)
                    ->setOwner($this->getReference($userRef))
                    ->setActive(true)
                    ->setLogoText($faker->catchPhrase)
                    ->setEmail($faker->companyEmail)
                    ->setTel($faker->phoneNumber)
                    ->setFax($faker->phoneNumber)
                    ->setPosition($inc)
                    ->setPitch($faker->sentence)
                ;
                $ref = 'company-'.$inc;
                $this->addReference($ref, $company);
                $manager->persist($company);
                $inc++;
            }
            $manager->flush();

        } catch (\Exception $e) {
            $this->container->get('logger')->error($e->getMessage());
        }
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 2;
    }
}