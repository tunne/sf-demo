<?php
/**
 * CountryController.php.
 */

namespace AppBundle\Controller\Api;

use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class CountryController.
 *
 * @Rest\View(templateVar="country")
 */
class CountryController extends FOSRestController
{
    private $em = null;

    /**
     * Constructor.
     */
    public function __construct()
    {
    }

    /**
     * Returns all countries.
     *
     * @Method({"GET"})
     *
     * @ApiDoc(
     *      section="Geo",
     *      Resource=false,
     *      Description="Retrieve list of countries.",
     *      filters={
     *          {"name"="limit", "dataType"="integer"},
     *          {"name"="offset", "dataType"="integer"}
     *      },
     *      requirements={
     *          {
     *              "name"="access_token",
     *              "dataType"="string"
     *          }
     *      },
     *      statusCodes={
     *          200="Returned when successful",
     *          403="Returned when you are not authorized",
     *          404={
     *              "Returned when the language is not found",
     *              "Returned when no news articles were found"
     *          },
     *          500="Returned when something went wrong"
     *      }
     * )
     *
     * @param Request      $request
     * @param ParamFetcher $pFetcher
     *
     * @return Rest\View
     *
     * @Rest\QueryParam(name="limit", requirements="\d+", default="0", description="Limit of the countries")
     * @Rest\QueryParam(name="offset", requirements="\d+", default="0", description="Limit of the countries")
     * @Rest\View(
     *  serializerGroups={"country_list"}
     * )
     *
     * @throws \Exception
     */
    public function indexAction(Request $request, ParamFetcher $pFetcher)
    {
        try {
            $rLimit = $pFetcher->get('limit');
            $rOffset = $pFetcher->get('offset');

            $limit = null;
            if ($rLimit) {
                $limit = (int) $rLimit;
            }

            $offset = null;
            if ($rOffset) {
                $offset = (int) $rOffset;
            }

            return $this->get('model_geo.repository.country')->findBy(array(), array('id' => 'desc'), $limit, $offset);
        } catch (\Exception $e) {
            if ($this->get('kernel')->getEnvironment() != 'prod') {
                return $this->view(sprintf('An exception occurred: %s, (file: %s), (line: %d)', $e->getMessage(), $e->getFile(), $e->getLine()), 500);
            }

            throw $e;
        }
    }

    /**
     * Returns a single country object.
     *
     * @Method({"GET"})
     *
     * @ApiDoc(
     *      section="Geo",
     *      Resource=false,
     *      Description="Returns a single country object by id",
     *      requirements={
     *          {
     *              "name"="id",
     *              "dataType"="integer",
     *              "description"="The country ID"
     *          },
     *          {
     *              "name"="access_token",
     *              "dataType"="string"
     *          }
     *      },
     *      statusCodes={
     *          200="Returned when successful",
     *          403="Returned when you are not authorized",
     *          404={
     *              "Returned when the language is not found",
     *              "Returned when no news articles were found"
     *          },
     *          500="Returned when something went wrong"
     *      }
     * )
     *
     * @param Request $request
     * @param $id
     *
     * @return mixed
     *
     * @Rest\View(
     *  serializerGroups={"country_show"}
     * )
     */
    public function showAction(Request $request, $id)
    {
        return $this->get('model_geo.repository.country')->find($id);
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectManager
     */
    private function getEm()
    {
        if ($this->em === null) {
            $this->em = $this->get('doctrine.orm.entity_manager');
        }

        return $this->em;
    }
}
