<?php
/**
 * AddressController.php.
 */

namespace AppBundle\Controller\Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View as RestView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Exception\NotFoundApiException;
use AppBundle\Form\Type\Api\AddressType;
use Model\Geo\AddressInterface;

/**
 * class AddressController.
 *
 * @Rest\View(templateVar="address")
 */
class AddressController extends FOSRestController
{
    private $em = null;

    /**
     * Constructor.
     */
    public function __construct()
    {
    }

    /**
     * Returns all addresses.
     *
     * @Method({"GET"})
     *
     * @ApiDoc(
     *      section="Geo",
     *      Resource=false,
     *      Description="Retrieve list of addresses.",
     *      filters={
     *          {"name"="limit", "dataType"="integer"},
     *          {"name"="offset", "dataType"="integer"}
     *      },
     *      requirements={
     *          {
     *              "name"="access_token",
     *              "dataType"="string"
     *          }
     *      },
     *      statusCodes={
     *          200="Returned when successful",
     *          403="Returned when you are not authorized",
     *          404={
     *              "Returned when the language is not found",
     *              "Returned when no news articles were found"
     *          },
     *          500="Returned when something went wrong"
     *      }
     * )
     *
     * @param Request      $request
     * @param ParamFetcher $pFetcher
     *
     * @return Rest\View
     *
     * @Rest\QueryParam(name="limit", requirements="\d+", default="0", description="Limit of the addresses")
     * @Rest\QueryParam(name="offset", requirements="\d+", default="0", description="Limit of the addresses")
     * @Rest\View(
     *  serializerGroups={"address_list"}
     * )
     *
     * @throws \Exception
     */
    public function indexAction(Request $request, ParamFetcher $pFetcher)
    {
        try {
            $rLimit = $pFetcher->get('limit');
            $rOffset = $pFetcher->get('offset');

            $limit = null;
            if ($rLimit) {
                $limit = (int) $rLimit;
            }

            $offset = null;
            if ($rOffset) {
                $offset = (int) $rOffset;
            }

            return $this->get('model_geo.repository.address')->findBy(array(), array('id' => 'desc'), $limit, $offset);
        } catch (\Exception $e) {
            if ($this->get('kernel')->getEnvironment() != 'prod') {
                return $this->view(sprintf('An exception occurred: %s, (file: %s), (line: %d)', $e->getMessage(), $e->getFile(), $e->getLine()), 500);
            }

            throw $e;
        }
    }

    /**
     * Creates an address.
     *
     * Returns the show method for the address
     *
     * @Method({"POST"})
     * @ApiDoc(
     *      section="Geo",
     *      Resource=false,
     *      Description="Creates an address",
     *      requirements={
     *          {
     *              "name"="company",
     *              "dataType"="int",
     *              "description"="Company id"
     *          },
     *          {
     *              "name"="department",
     *              "dataType"="int",
     *              "description"="Department id"
     *          },
     *          {
     *              "name"="Street",
     *              "dataType"="string",
     *              "description"="The street"
     *          },
     *          {
     *              "name"="nr",
     *              "dataType"="string",
     *              "description"="The number"
     *          },
     *          {
     *              "name"="city",
     *              "dataType"="int",
     *              "description"="The city id"
     *          },
     *          {
     *              "name"="postcode",
     *              "dataType"="string",
     *              "description"="The postcode"
     *          },
     *          {
     *              "name"="country",
     *              "dataType"="int",
     *              "description"="The country id"
     *          },
     *          {
     *              "name"="province",
     *              "dataType"="int",
     *              "description"="The province id"
     *          },
     *          {
     *              "name"="marker",
     *              "dataType"="int",
     *              "description"="The marker id"
     *          }
     *      },
     *      statusCodes={
     *          200="Returned when successful",
     *          403="Returned when you are not authorized",
     *          404={
     *              "Returned when the language is not found",
     *              "Returned when no news articles were found"
     *          },
     *          500="Returned when something went wrong"
     *      }
     * )
     *
     * @param Request $request
     *
     * @return \FOS\RestBundle\View\View
     *
     * @throws \Exception
     */
    public function createAction(Request $request)
    {
        try {
            $address = $this->get('model_geo.repository.address')->createNew();
            $form = $this->get('form.factory')->createNamed('', new AddressType(), $address);

            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->getEm()->persist($address);
                $this->getEm()->flush();

                return $this->redirectView(
                    $this->generateUrl(
                        'app_api_address_show',
                        array('id' => $address->getId())
                    ),
                    301
                );
            }

            return $form;
        } catch (\Exception $e) {
            if ($this->get('kernel')->getEnvironment() != 'prod') {
                return $this->view(sprintf('An exception occurred: %s, (file: %s), (line: %d)', $e->getMessage(), $e->getFile(), $e->getLine()), 500);
            }

            throw $e;
        }
    }

    /**
     * Returns a single address object.
     *
     * @Method({"GET"})
     *
     * @ApiDoc(
     *      section="Geo",
     *      Resource=false,
     *      Description="Returns a single address object by id",
     *      requirements={
     *          {
     *              "name"="id",
     *              "dataType"="integer",
     *              "description"="The address ID"
     *          },
     *          {
     *              "name"="access_token",
     *              "dataType"="string"
     *          }
     *      },
     *      statusCodes={
     *          200="Returned when successful",
     *          403="Returned when you are not authorized",
     *          404={
     *              "Returned when the language is not found",
     *              "Returned when no news articles were found"
     *          },
     *          500="Returned when something went wrong"
     *      }
     * )
     *
     * @param Request      $request
     * @param ParamFetcher $pFetcher
     * @param $id
     *
     * @return mixed
     *
     * @Rest\View(
     *  serializerGroups={"address_show"}
     * )
     */
    public function showAction(Request $request, ParamFetcher $pFetcher, $id)
    {
        return $this->get('model_geo.repository.address')->find($id);
    }

    /**
     * Updates an address object.
     *
     * @Method({"PUT"})
     *
     * @ApiDoc(
     *      section="Geo",
     *      Resource=false,
     *      Description="Returns a single address object by id",
     *      requirements={
     *          {
     *              "name"="id",
     *              "dataType"="integer",
     *              "description"="The address ID"
     *          },
     *          {
     *              "name"="access_token",
     *              "dataType"="string"
     *          }
     *      },
     *      statusCodes={
     *          200="Returned when successful",
     *          403="Returned when you are not authorized",
     *          404={
     *              "Returned when the language is not found",
     *              "Returned when no news articles were found"
     *          },
     *          500="Returned when something went wrong"
     *      }
     * )
     *
     * @param Request $request
     * @param $id
     *
     * @return mixed
     */
    public function updateAction(Request $request, $id)
    {
        $address = $this->get('model_geo.repository.address')->find($id);

        if (!$address) {
            throw new NotFoundHttpException('Address not found!');
        }

        /** @var \Symfony\Component\Form\Form $form */
        $form = $this->get('form.factory')->createNamed('', new AddressType(), $address);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getEm()->persist($address);
            $this->getEm()->flush();

            return $this->redirectView(
                $this->generateUrl(
                    'app_api_address_show',
                    array('id' => $address->getId())
                ),
                301
            );
        }

        return $form;
    }

    /**
     * @param Request $request
     * @param $id
     *
     * @Rest\View(statusCode=204)
     *
     * @return RestView|void
     *
     * @throws \Exception
     */
    public function deleteAction(Request $request, $id)
    {
        try {
            $address = $this->get('model_geo.repository.address')->find($id);
            if (!$address instanceof AddressInterface) {
                throw new NotFoundApiException('Address not found for removal');
            }

            $this->getEm()->remove($address);
            $this->getEm()->flush();

            return;
        } catch (\Exception $e) {
            if ($this->get('kernel')->getEnvironment() != 'prod') {
                return $this->view(sprintf('An exception occurred: %s, (file: %s), (line: %d)', $e->getMessage(), $e->getFile(), $e->getLine()), 500);
            }

            if ($e instanceof NotFoundApiException) {
                return $this->view('Address not found for removal', 500);
            }

            throw $e;
        }
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectManager
     */
    private function getEm()
    {
        if ($this->em === null) {
            $this->em = $this->get('doctrine.orm.entity_manager');
        }

        return $this->em;
    }
}
