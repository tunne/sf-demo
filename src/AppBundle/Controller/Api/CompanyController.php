<?php
/**
 * CompanyController.php.
 */

namespace AppBundle\Controller\Api;

use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View as RestView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Exception\NotFoundApiException;
use AppBundle\Form\Type\Api\CompanyType;
use Model\Business\CompanyInterface;

/**
 * Class CompanyController.
 *
 * @Rest\View(templateVar="company")
 */
class CompanyController extends FOSRestController
{
    /**
     * indexAction: Returns all companies.
     *
     * @Method({"GET"})
     *
     * @ApiDoc(
     *      section="Business",
     *      Resource=false,
     *      Description="Retrieve list of companies.",
     *      filters={
     *          {"name"="limit", "dataType"="integer"},
     *          {"name"="offset", "dataType"="integer"}
     *      },
     *      requirements={
     *          {
     *              "name"="access_token",
     *              "dataType"="string"
     *          }
     *      },
     *      statusCodes={
     *          200="Returned when successful",
     *          403="Returned when you are not authorized",
     *          404={
     *              "Returned when the language is not found",
     *              "Returned when no news articles were found"
     *          },
     *          500="Returned when something went wrong"
     *      }
     * )
     *
     * @param Request      $request
     * @param ParamFetcher $pFetcher
     *
     * @return Rest\View
     *
     * @Rest\QueryParam(name="limit", requirements="\d+", default="0", description="Limit of the companies")
     * @Rest\QueryParam(name="offset", requirements="\d+", default="0", description="Limit of the companies")
     * @Rest\View(
     *  serializerGroups={"company_list"}
     * )
     *
     * @throws \Exception
     */
    public function indexAction(Request $request, ParamFetcher $pFetcher)
    {
        try {
            $rLimit = $pFetcher->get('limit');
            $rOffset = $pFetcher->get('offset');

            $limit = null;
            if ($rLimit) {
                $limit = (int) $rLimit;
            }

            $offset = null;
            if ($rOffset) {
                $offset = (int) $rOffset;
            }

            return $this->get('model_business.repository.company')->findBy(array(), array('id' => 'desc'), $limit, $offset);
        } catch (\Exception $e) {
            if ($this->get('kernel')->getEnvironment() != 'prod') {
                return $this->view(sprintf('An exception occurred: %s, (file: %s), (line: %d)', $e->getMessage(), $e->getFile(), $e->getLine()), 500);
            }

            throw $e;
        }
    }

    /**
     * showAction: returns a company by id.
     *
     * @Method({"GET"})
     * @ApiDoc(
     *      section="Business",
     *      Resource=false,
     *      Description="Returns a single company object by id",
     *      requirements={
     *          {
     *              "name"="id",
     *              "dataType"="integer",
     *              "description"="The company ID"
     *          },
     *          {
     *              "name"="access_token",
     *              "dataType"="string"
     *          }
     *      },
     *      statusCodes={
     *          200="Returned when successful",
     *          403="Returned when you are not authorized",
     *          404={
     *              "Returned when the language is not found",
     *              "Returned when no news articles were found"
     *          },
     *          500="Returned when something went wrong"
     *      }
     * )
     *
     * @param Request $request
     * @param $id
     *
     * @return mixed
     *
     * @Rest\View(
     *  serializerGroups={"company_show"}
     * )
     */
    public function showAction(Request $request, $id)
    {
        return $this->get('model_business.repository.company')->find($id);
    }

    public function createAction(Request $request)
    {
        try {
            $company = $this->get('model_business.repository.company')->createNew();
            $form = $this->get('form.factory')->createNamed('', new CompanyType(), $company);

            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->get('doctrine.orm.entity_manager')->persist($company);
                $this->get('doctrine.orm.entity_manager')->flush();

                return $this->redirectView(
                    $this->generateUrl(
                        'app_api_company_show',
                        array('id' => $company->getId())
                    ),
                    301
                );
            }

            return $form;
        } catch (\Exception $e) {
            if ($this->get('kernel')->getEnvironment() != 'prod') {
                return $this->view(sprintf('An exception occurred: %s, (file: %s), (line: %d)', $e->getMessage(), $e->getFile(), $e->getLine()), 500);
            }

            throw $e;
        }
    }

    /**
     * updateAction: Updates a company object.
     *
     * @Method({"PUT"})
     *
     * @ApiDoc(
     *      section="Business",
     *      Resource=false,
     *      Description="Updates a company object",
     *      requirements={
     *          {
     *              "name"="id",
     *              "dataType"="integer",
     *              "description"="The company ID"
     *          },
     *          {
     *              "name"="access_token",
     *              "dataType"="string"
     *          }
     *      },
     *      statusCodes={
     *          200="Returned when successful",
     *          403="Returned when you are not authorized",
     *          404={
     *              "Returned when the language is not found",
     *              "Returned when no news articles were found"
     *          },
     *          500="Returned when something went wrong"
     *      }
     * )
     *
     * @param Request $request
     * @param $id
     *
     * @return RestView|\Symfony\Component\Form\Form
     *
     * @throws NotFoundApiException
     */
    public function updateAction(Request $request, $id)
    {
        $company = $this->get('model_business.repository.company')->find($id);

        if (!$company) {
            throw new NotFoundApiException('Company not found!');
        }

        /** @var \Symfony\Component\Form\Form $form */
        $form = $this->get('form.factory')->createNamed('', new CompanyType(), $company);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->get('doctrine.orm.entity_manager')->persist($company);
            $this->get('doctrine.orm.entity_manager')->flush();

            return $this->redirectView(
                $this->generateUrl(
                    'app_api_company_show',
                    array('id' => $company->getId())
                ),
                301
            );
        }

        return $form;
    }

    /**
     * deleteAction: Deletes a company object.
     *
     * @Method({"DELETE"})
     *
     * @ApiDoc(
     *      section="Business",
     *      Resource=false,
     *      Description="Deletes a company object",
     *      requirements={
     *          {
     *              "name"="id",
     *              "dataType"="integer",
     *              "description"="The company ID"
     *          },
     *          {
     *              "name"="access_token",
     *              "dataType"="string"
     *          }
     *      },
     *      statusCodes={
     *          200="Returned when successful",
     *          403="Returned when you are not authorized",
     *          404={
     *              "Returned when the language is not found",
     *              "Returned when no news articles were found"
     *          },
     *          500="Returned when something went wrong"
     *      }
     * )
     *
     * @param Request $request
     * @param $id
     *
     * @return RestView|\Symfony\Component\Form\Form
     *
     * @throws \Exception
     *
     * @Rest\View(statusCode=204)
     */
    public function deleteAction(Request $request, $id)
    {
        try {
            $company = $this->get('model_business.repository.company')->find($id);
            if (!$company instanceof CompanyInterface) {
                throw new NotFoundApiException('Company not found for removal');
            }

            $this->get('doctrine.orm.entity_manager')->remove($company);
            $this->get('doctrine.orm.entity_manager')->flush();

            return;
        } catch (\Exception $e) {
            if ($this->get('kernel')->getEnvironment() != 'prod') {
                return $this->view(sprintf('An exception occurred: %s, (file: %s), (line: %d)', $e->getMessage(), $e->getFile(), $e->getLine()), 500);
            }

            if ($e instanceof NotFoundApiException) {
                return $this->view('Company not found for removal', 500);
            }

            throw $e;
        }
    }
}
