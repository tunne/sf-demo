<?php
/**
 * UserController.php.
 */

namespace AppBundle\Controller\Api;

use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class CompanyController.
 *
 * @Rest\View(templateVar="user")
 */
class UserController extends FOSRestController
{
    /**
     * showAction: returns a company by id.
     *
     * @Method({"GET"})
     * @ApiDoc(
     *      section="Business",
     *      Resource=false,
     *      Description="Returns a single company object by id",
     *      requirements={
     *          {
     *              "name"="id",
     *              "dataType"="integer",
     *              "description"="The company ID"
     *          },
     *          {
     *              "name"="access_token",
     *              "dataType"="string"
     *          }
     *      },
     *      statusCodes={
     *          200="Returned when successful",
     *          403="Returned when you are not authorized",
     *          404={
     *              "Returned when the language is not found",
     *              "Returned when no news articles were found"
     *          },
     *          500="Returned when something went wrong"
     *      }
     * )
     *
     * @param Request $request
     * @param $id
     *
     * @return mixed
     *
     * @Rest\View(
     *  serializerGroups={"user_show"}
     * )
     */
    public function showAction(Request $request, $id)
    {
        return $this->get('model_auth.repository.user')->find($id);
    }
}
