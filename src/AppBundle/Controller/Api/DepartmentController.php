<?php
/**
 * DepartmentController.php.
 */

namespace AppBundle\Controller\Api;

use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View as RestView;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Exception\NotFoundApiException;
use AppBundle\Form\Type\Api\DepartmentType;
use Model\Business\DepartmentInterface;

/**
 * Class DepartmentController.
 *
 * @Rest\View(templateVar="department")
 */
class DepartmentController extends FOSRestController
{
    /**
     * indexAction: Returns all departments.
     *
     * @Method({"GET"})
     *
     * @ApiDoc(
     *      section="Business",
     *      Resource=false,
     *      Description="Retrieve list of departments.",
     *      filters={
     *          {"name"="limit", "dataType"="integer"},
     *          {"name"="offset", "dataType"="integer"}
     *      },
     *      requirements={
     *          {
     *              "name"="access_token",
     *              "dataType"="string"
     *          }
     *      },
     *      statusCodes={
     *          200="Returned when successful",
     *          403="Returned when you are not authorized",
     *          404={
     *              "Returned when the language is not found",
     *              "Returned when no news articles were found"
     *          },
     *          500="Returned when something went wrong"
     *      }
     * )
     *
     * @param Request      $request
     * @param ParamFetcher $pFetcher
     *
     * @return Rest\View
     *
     * @Rest\QueryParam(name="limit", requirements="\d+", default="0", description="Limit of the departments")
     * @Rest\QueryParam(name="offset", requirements="\d+", default="0", description="Limit of the departments")
     * @Rest\View(
     *  serializerGroups={"department_list"}
     * )
     *
     * @throws \Exception
     */
    public function indexAction(Request $request, ParamFetcher $pFetcher)
    {
        try {
            $rLimit = $pFetcher->get('limit');
            $rOffset = $pFetcher->get('offset');

            $limit = null;
            if ($rLimit) {
                $limit = (int) $rLimit;
            }

            $offset = null;
            if ($rOffset) {
                $offset = (int) $rOffset;
            }

            return $this->get('model_business.repository.department')->findBy(array(), array('id' => 'desc'), $limit, $offset);
        } catch (\Exception $e) {
            if ($this->get('kernel')->getEnvironment() != 'prod') {
                return $this->view(sprintf('An exception occurred: %s, (file: %s), (line: %d)', $e->getMessage(), $e->getFile(), $e->getLine()), 500);
            }

            throw $e;
        }
    }

    /**
     * showAction: returns a department by id.
     *
     * @Method({"GET"})
     * @ApiDoc(
     *      section="Business",
     *      Resource=false,
     *      Description="Returns a single department object by id",
     *      requirements={
     *          {
     *              "name"="id",
     *              "dataType"="integer",
     *              "description"="The department ID"
     *          },
     *          {
     *              "name"="access_token",
     *              "dataType"="string"
     *          }
     *      },
     *      statusCodes={
     *          200="Returned when successful",
     *          403="Returned when you are not authorized",
     *          404={
     *              "Returned when the language is not found",
     *              "Returned when no news articles were found"
     *          },
     *          500="Returned when something went wrong"
     *      }
     * )
     *
     * @param Request $request
     * @param $id
     *
     * @return mixed
     *
     * @Rest\View(
     *  serializerGroups={"department_show"}
     * )
     */
    public function showAction(Request $request, $id)
    {
        return $this->get('model_business.repository.department')->find($id);
    }

    public function createAction(Request $request)
    {
        try {
            $department = $this->get('model_business.repository.department')->createNew();
            $form = $this->get('form.factory')->createNamed('', new DepartmentType(), $department);

            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->get('doctrine.orm.entity_manager')->persist($department);
                $this->get('doctrine.orm.entity_manager')->flush();

                return $this->redirectView(
                    $this->generateUrl(
                        'app_api_department_show',
                        array('id' => $department->getId())
                    ),
                    301
                );
            }

            return $form;
        } catch (\Exception $e) {
            if ($this->get('kernel')->getEnvironment() != 'prod') {
                return $this->view(sprintf('An exception occurred: %s, (file: %s), (line: %d)', $e->getMessage(), $e->getFile(), $e->getLine()), 500);
            }

            throw $e;
        }
    }

    /**
     * updateAction: Updates a department object.
     *
     * @Method({"PUT"})
     *
     * @ApiDoc(
     *      section="Business",
     *      Resource=false,
     *      Description="Updates a department object",
     *      requirements={
     *          {
     *              "name"="id",
     *              "dataType"="integer",
     *              "description"="The department ID"
     *          },
     *          {
     *              "name"="access_token",
     *              "dataType"="string"
     *          }
     *      },
     *      statusCodes={
     *          200="Returned when successful",
     *          403="Returned when you are not authorized",
     *          404={
     *              "Returned when the language is not found",
     *              "Returned when no news articles were found"
     *          },
     *          500="Returned when something went wrong"
     *      }
     * )
     *
     * @param Request $request
     * @param $id
     *
     * @return RestView|\Symfony\Component\Form\Form
     *
     * @throws NotFoundApiException
     */
    public function updateAction(Request $request, $id)
    {
        $department = $this->get('model_business.repository.department')->find($id);

        if (!$department) {
            throw new NotFoundApiException('Department not found!');
        }

        /** @var \Symfony\Component\Form\Form $form */
        $form = $this->get('form.factory')->createNamed('', new DepartmentType(), $department);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->get('doctrine.orm.entity_manager')->persist($department);
            $this->get('doctrine.orm.entity_manager')->flush();

            return $this->redirectView(
                $this->generateUrl(
                    'app_api_department_show',
                    array('id' => $department->getId())
                ),
                301
            );
        }

        return $form;
    }

    /**
     * deleteAction: Deletes a department object.
     *
     * @Method({"DELETE"})
     *
     * @ApiDoc(
     *      section="Business",
     *      Resource=false,
     *      Description="Deletes a department object",
     *      requirements={
     *          {
     *              "name"="id",
     *              "dataType"="integer",
     *              "description"="The department ID"
     *          },
     *          {
     *              "name"="access_token",
     *              "dataType"="string"
     *          }
     *      },
     *      statusCodes={
     *          200="Returned when successful",
     *          403="Returned when you are not authorized",
     *          404={
     *              "Returned when the language is not found",
     *              "Returned when no news articles were found"
     *          },
     *          500="Returned when something went wrong"
     *      }
     * )
     *
     * @param Request $request
     * @param $id
     *
     * @return RestView|\Symfony\Component\Form\Form
     *
     * @throws \Exception
     *
     * @Rest\View(statusCode=204)
     */
    public function deleteAction(Request $request, $id)
    {
        try {
            $department = $this->get('model_business.repository.department')->find($id);
            if (!$department instanceof DepartmentInterface) {
                throw new NotFoundApiException('Department not found for removal');
            }

            $this->get('doctrine.orm.entity_manager')->remove($department);
            $this->get('doctrine.orm.entity_manager')->flush();

            return;
        } catch (\Exception $e) {
            if ($this->get('kernel')->getEnvironment() != 'prod') {
                return $this->view(sprintf('An exception occurred: %s, (file: %s), (line: %d)', $e->getMessage(), $e->getFile(), $e->getLine()), 500);
            }

            if ($e instanceof NotFoundApiException) {
                return $this->view('Department not found for removal', 500);
            }

            throw $e;
        }
    }
}
