<?php
/**
 * BoxController.php.
 */

namespace AppBundle\Controller\Frontend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BoxController.
 */
class BoxController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function navBoxAction(Request $request)
    {
        $categories = $this->get('model_classification.repository.category')->findCategoriesForIndex();
        $tags = $this->get('model_classification.repository.tag')->findTagsForIndex();
        $companies = $this->get('model_business.repository.company')->findCompaniesForIndex();

        return $this->render('AppBundle:Frontend\Box:navbox.html.twig', array(
            'categories' => $categories,
            'tags' => $tags,
            'companies' => $companies,
        ));
    }
}