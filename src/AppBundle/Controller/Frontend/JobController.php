<?php
/**
 * JobController.php.
 */

namespace AppBundle\Controller\Frontend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class JobController.
 */
class JobController extends Controller
{
    /**
     * @param Request $request
     * @param $slug
     * @return Response
     */
    public function indexByTagAction(Request $request, $slug)
    {
        $tag = $this->get('model_classification.repository.tag')->findOneBySlug($slug);
        $jobs = $this->get('model_business.repository.job')->findByTagSlug($slug);

        return $this->render('AppBundle:Frontend\Job:tag_index.html.twig', array(
            'tag' => $tag,
            'jobs' => $jobs,
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function showAction(Request $request, $id)
    {
        $job = $this->get('model_business.repository.job')->find($id);
        return $this->render('AppBundle:Frontend\Job:show.html.twig', array(
            'job' => $job,
        ));
    }
}
