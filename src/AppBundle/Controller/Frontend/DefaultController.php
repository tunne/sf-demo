<?php
/**
 * DefaultController.php.
 */

namespace AppBundle\Controller\Frontend;


use Model\Business\CompanyInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Model\Business\JobInterface;
use Model\Geo\Marker;

/**
 * Class DefaultController.
 */
class DefaultController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $gMap = $this->getMap();
        return $this->render('AppBundle:Frontend\Default:index.html.twig', array(
            'gmap' => $gMap,
        ));
    }

    /**
     * @return object
     */
    private function getMap()
    {
        $companies = $this->get('model_business.repository.company')->findAllFrontendCompanies();
        $map = $this->get('ivory_google_map.map');

        foreach ($companies as $company) {
            if ($marker = $this->createMarker($company)) {
                $map->addMarker($marker);
            }
        }

        return $map;
    }

    /**
     * @param CompanyInterface $company
     * @return bool|object
     */
    private function createMarker(CompanyInterface $company)
    {
        if (!$address = $company->getAddress()) {
            return false;
        }

        if (!$companyMarker = $address->getMarker()) {
            return false;
        }

        $marker = $this->get('ivory_google_map.marker');
        $marker->setPosition($companyMarker->getLat(), $companyMarker->getLng(), true);
        $marker->setInfoWindow($this->createInfoWindow($company));

        return $marker;
    }

    /**
     * @param CompanyInterface $company
     * @return object
     */
    private function createInfoWindow(CompanyInterface $company)
    {
        $infoWindow = $this->get('ivory_google_map.info_window');
        $infoWindow->setContent($this->renderView('AppBundle:Frontend\Marker:info.html.twig', array(
            'company' => $company,
        )));
        return $infoWindow;
    }
}
