<?php
/**
 * CompanyController.php.
 */

namespace AppBundle\Controller\Frontend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Model\Business\CompanyInterface;

/**
 * Class CompanyController.
 */
class CompanyController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $companies = $this->get('model_business.repository.company')->findCompaniesForIndex();
        return $this->render('AppBundle:Frontend\Company:index.html.twig', array(
            'companies' => $companies,
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function showAction(Request $request, $id)
    {
        $company = $this->get('model_business.repository.company')->find($id);

        if (!$gMarker = $this->createMarker($company)) {
            return $this->render('AppBundle:Frontend\Company:show.html.twig', array(
                'company' => $company,
            ));
        }

        $map = $this->get('ivory_google_map.map');
        $map->addMarker($gMarker);
        $map->setMapOption('zoom', 12);
        $map->setAutoZoom(false);
        $map->setCenter($company->getAddress()->getMarker()->getLat(), $company->getAddress()->getMarker()->getLng(), true);

        return $this->render('AppBundle:Frontend\Company:show.html.twig', array(
            'company' => $company,
            'gmap' => $map,
        ));
    }

    /**
     * @param CompanyInterface $company
     * @return bool|object
     */
    private function createMarker(CompanyInterface $company)
    {
        if (!$address = $company->getAddress()) {
            return false;
        }

        if (!$companyMarker = $address->getMarker()) {
            return false;
        }

        $marker = $this->get('ivory_google_map.marker');
        $marker->setPosition($companyMarker->getLat(), $companyMarker->getLng(), true);
        $marker->setInfoWindow($this->createInfoWindow($company));

        return $marker;
    }

    /**
     * @param CompanyInterface $company
     * @return object
     */
    private function createInfoWindow(CompanyInterface $company)
    {
        $infoWindow = $this->get('ivory_google_map.info_window');
        $infoWindow->setContent($this->renderView('AppBundle:Frontend\Marker:info.html.twig', array(
            'company' => $company,
        )));
        return $infoWindow;
    }
}