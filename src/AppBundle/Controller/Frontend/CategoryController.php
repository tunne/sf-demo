<?php
/**
 * CategoryController.php.
 */

namespace AppBundle\Controller\Frontend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CategoryController.
 */
class CategoryController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $categories = $this->get('model_classification.repository.category')->findCategoriesForIndex();
        return $this->render('AppBundle:Frontend\Category:index.html.twig', array(
            'categories' => $categories,
        ));
    }

    /**
     * @param Request $request
     * @param $slug
     * @return Response
     */
    public function showAction(Request $request, $slug)
    {
        $category = $this->get('model_classification.repository.category')->findOneBySlug($slug);
        return $this->render('AppBundle:Frontend\Category:show.html.twig', array(
            'category' => $category,
        ));
    }
}