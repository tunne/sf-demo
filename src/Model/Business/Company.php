<?php
/**
 * Company.php.
 */

namespace Model\Business;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Model\Auth\User;
use Model\Geo\Address;

/**
 * Class Company.
 *
 * @ORM\Table(name="business_company")
 * @ORM\Entity(repositoryClass="Model\Business\Repository\CompanyRepository")
 *
 * @Gedmo\Loggable
 *
 * @ExclusionPolicy("all")
 */
class Company implements CompanyInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     * @SerializedName("id")
     *
     * @Groups({"company_list", "company_show"})
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", name="active")
     *
     * @Gedmo\Versioned
     *
     * @Expose
     * @SerializedName("active")
     * @Groups({"company_list", "company_show"})
     *
     */
    private $active;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", name="position")
     *
     * @Gedmo\Versioned
     * @Gedmo\SortablePosition
     *
     * @Expose
     * @SerializedName("position")
     * @Groups({"company_list", "company_show"})
     *
     * @Assert\NotBlank()
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true, name="name")
     *
     * @Gedmo\Versioned
     *
     * @Expose
     * @SerializedName("name")
     * @Groups({"company_list", "company_show"})
     *
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true, name="logo_text")
     *
     * @Gedmo\Versioned
     * @Groups({"company_show"})
     *
     * @Expose
     * @SerializedName("logo_text")
     */
    private $logoText;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true, name="pitch")
     *
     * @Gedmo\Versioned
     *
     * @Expose
     * @SerializedName("baseline")
     * @Groups({"company_show"})
     *
     * @Assert\NotBlank()
     */
    private $pitch;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=80, name="email")
     *
     * @Expose
     * @SerializedName("email")
     * @Groups({"company_list", "company_show"})
     *
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=80, name="tel")
     *
     * @Expose
     * @SerializedName("telephone")
     * @Groups({"company_list", "company_show"})
     *
     * @Assert\NotBlank()
     */
    private $tel;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=80, name="fax", nullable=true)
     *
     * @Expose
     * @SerializedName("fax")
     * @Groups({"company_show"})
     */
    private $fax;

    /**
     * @var ArrayCollection
     *
     * @ORM\Column(type="string", length=80, name="vat", nullable=true)
     *
     * @Expose
     * @SerializedName("vat")
     * @Groups({"company_show"})
     *
     */
    private $vat;

    /**
     * @var User
     *
     *
     * @ORM\ManyToOne(targetEntity="Model\Auth\User", inversedBy="companies", cascade={"persist"})
     * @ORM\JoinColumn(name="owner", referencedColumnName="id")
     *
     * @Gedmo\Versioned
     * @Gedmo\SortableGroup
     *
     * @Expose
     * @SerializedName("owner")
     * @Groups({"company_show"})
     */
    private $owner;

    /**
     * @var Address
     *
     * @ORM\OneToOne(targetEntity="Model\Geo\Address", mappedBy="company", cascade={"persist"}, orphanRemoval=false)
     *
     * @Gedmo\Versioned
     *
     * @Expose
     * @SerializedName("address")
     * @Groups({"company_list", "company_show", "job_show"})
     */
    private $address;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Model\Business\Department", mappedBy="company", cascade={"persist", "remove"})
     *
     * @Expose
     * @SerializedName("departments")
     * @Groups({"company_show"})
     */
    protected $departments;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Model\Business\Job", mappedBy="company", cascade={"persist", "remove"})
     *
     * @Expose
     * @SerializedName("jobs")
     * @Groups({"company_show"})
     */
    protected $jobs;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", name="created_at")
     *
     * @Gedmo\Timestampable(on="create")
     *
     * @Expose
     * @SerializedName("created_at")
     * @Groups({"company_show"})
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", name="updated_at")
     *
     * @Gedmo\Timestampable(on="change")
     *
     * @Expose
     * @SerializedName("updated_at")
     * @Groups({"company_show"})
     */
    private $updatedAt;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->active = false;
        $this->position = 0;
        $this->departments = new ArrayCollection();
        $this->jobs = new ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @param $active
     *
     * @return $this
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param int $position
     * @return $this
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $logoText
     *
     * @return $this
     */
    public function setLogoText($logoText)
    {
        $this->logoText = $logoText;

        return $this;
    }

    /**
     * @return string
     */
    public function getLogoText()
    {
        return $this->logoText;
    }

    /**
     * @param $pitch
     *
     * @return $this
     */
    public function setPitch($pitch)
    {
        $this->pitch = $pitch;

        return $this;
    }

    /**
     * @return string
     */
    public function getPitch()
    {
        return $this->pitch;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $fax
     * @return $this
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param $owner
     *
     * @return $this
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return \Model\Auth\User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param $tel
     *
     * @return $this
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * @param string $vat
     *
     * @return $this
     */
    public function setVat($vat)
    {
        $this->vat = $vat;

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param ArrayCollection $departments
     *
     * @return $this
     */
    public function setDepartments(ArrayCollection $departments)
    {
        $this->departments = $departments;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getDepartments()
    {
        return $this->departments;
    }

    /**
     * @param DepartmentInterface $department
     *
     * @return bool
     */
    public function hasDepartment(DepartmentInterface $department)
    {
        if ($this->departments->contains($department)) {
            return true;
        }

        return false;
    }

    /**
     * @param $department
     *
     * @return $this
     */
    public function addDepartment(DepartmentInterface $department)
    {
        if (!$this->hasDepartment($department)) {
            $this->departments[] = $department;
        }

        return $this;
    }

    /**
     * @param $department
     *
     * @return $this
     */
    public function removeDepartment(DepartmentInterface $department)
    {
        if ($this->hasDepartment($department)) {
            $this->departments->removeElement($department);
        }

        return $this;
    }

    /**
     * @param ArrayCollection $jobs
     *
     * @return $this
     */
    public function setJobs(ArrayCollection $jobs)
    {
        $this->jobs = $jobs;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getJobs()
    {
        return $this->jobs;
    }

    /**
     * @param JobInterface $job
     *
     * @return bool
     */
    public function hasJob(JobInterface $job)
    {
        if ($this->jobs->contains($job)) {
            return true;
        }

        return false;
    }

    /**
     * @param JobInterface $job
     *
     * @return $this
     */
    public function addJob(JobInterface $job)
    {
        if (!$this->hasJob($job)) {
            $this->jobs[] = $job;
            $job->setCompany($this);
        }

        return $this;
    }

    /**
     * @param JobInterface $job
     *
     * @return $this
     */
    public function removeJob(JobInterface $job)
    {
        foreach ($this->getDepartments() as $department) {
            $department->removeJob($job);
        }

        if ($this->hasJob($job)) {
            $this->jobs->removeElement($job);
        }

        return $this;
    }

    /**
     * @param Address $address
     *
     * @return $this
     */
    public function setAddress($address)
    {
        if ($this->address) {
            $this->address->setCompany(null);
        }
        if ($address) {
            $address->setCompany($this);
        }
        $this->address = $address;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
