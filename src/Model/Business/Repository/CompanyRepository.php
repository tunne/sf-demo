<?php
/**
 * CompanyRepository.php.
 */

namespace Model\Business\Repository;

use Doctrine\ORM\EntityRepository;
use Model\Business\Company;

/**
 * Class CompanyRepository.
 */
class CompanyRepository extends EntityRepository implements CompanyRepositoryInterface
{
    /**
     * @return Company
     */
    public function createNew()
    {
        return new Company();
    }

    /**
     * @return array
     */
    public function findAllFrontendCompanies()
    {
        return $this->createQueryBuilder('c')
            ->join('c.address', 'ca')
            ->join('ca.marker', 'cam')
            ->join('c.jobs', 'aj')
            ->andWhere('cam.lat IS NOT NULL')
            ->andWhere('cam.lng IS NOT NULL')
            ->andWhere('c.active = true')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return array
     */
    public function findCompaniesForIndex()
    {
        return $this->createQueryBuilder('c')
            ->join('c.jobs', 'aj')
            ->andWhere('c.active = true')
            ->getQuery()
            ->getResult()
        ;
    }
}
