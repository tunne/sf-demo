<?php
/**
 * DepartmentRepository.php.
 */

namespace Model\Business\Repository;

use Doctrine\ORM\EntityRepository;
use Model\Business\Department;

/**
 * Class DepartmentRepository.
 */
class DepartmentRepository extends EntityRepository implements DepartmentRepositoryInterface
{
    public function createNew()
    {
        return new Department();
    }
}
