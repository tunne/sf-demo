<?php
/**
 * JobRepository.php.
 */

namespace Model\Business\Repository;

use Doctrine\ORM\EntityRepository;
use Model\Business\Job;

/**
 * Class JobRepository.
 */
class JobRepository extends EntityRepository implements JobRepositoryInterface
{
    public function createNew()
    {
        return new Job();
    }

    /**
     * @return array
     */
    public function getJobsWithCompany()
    {
        return  $this
            ->createQueryBuilder('j')
            ->leftJoin('j.company', 'jc')
            ->leftJoin('j.department', 'jd')
            ->andWhere('j.company IS NOT NULL')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param $slug
     * @return array
     */
    public function findByTagSlug($slug)
    {
        return $this->createQueryBuilder('j')
            ->join('j.tags', 'jt')
            ->andWhere('jt.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getResult()
            ;
    }
}
