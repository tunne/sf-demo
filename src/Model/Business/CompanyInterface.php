<?php
/**
 * CompanyInterface.php.
 */

namespace Model\Business;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Interface CompanyInterface.
 */
interface CompanyInterface
{
    /**
     * @param $active
     *
     * @return $this
     */
    public function setActive($active);

    /**
     * @return boolean
     */
    public function getActive();

    /**
     * @param int $position
     */
    public function setPosition($position);

    /**
     * @return int
     */
    public function getPosition();

    /**
     * @return int
     */
    public function getId();

    /**
     * @param $name
     *
     * @return $this
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getName();
    /**
     * @param $logoText
     *
     * @return $this
     */
    public function setLogoText($logoText);

    /**
     * @return string
     */
    public function getLogoText();

    /**
     * @param $pitch
     *
     * @return $this
     */
    public function setPitch($pitch);

    /**
     * @return string
     */
    public function getPitch();

    /**
     * @param string $email
     */
    public function setEmail($email);

    /**
     * @return string
     */
    public function getEmail();

    /**
     * @param string $fax
     */
    public function setFax($fax);

    /**
     * @return string
     */
    public function getFax();

    /**
     * @param $owner
     *
     * @return $this
     */
    public function setOwner($owner);

    /**
     * @return \Model\Auth\User
     */
    public function getOwner();

    /**
     * @param $tel
     *
     * @return $this
     */
    public function setTel($tel);

    /**
     * @return string
     */
    public function getTel();

    /**
     * @param string $vat
     *
     * @return $this
     */
    public function setVat($vat);
    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getVat();

    /**
     * @param ArrayCollection $departments
     *
     * @return $this
     */
    public function setDepartments(ArrayCollection $departments);

    /**
     * @return ArrayCollection
     */
    public function getDepartments();

    /**
     * @param DepartmentInterface $department
     *
     * @return bool
     */
    public function hasDepartment(DepartmentInterface $department);

    /**
     * @param $department
     *
     * @return $this
     */
    public function addDepartment(DepartmentInterface $department);

    /**
     * @param $department
     *
     * @return $this
     */
    public function removeDepartment(DepartmentInterface $department);

    /**
     * @param ArrayCollection $jobs
     *
     * @return $this
     */
    public function setJobs(ArrayCollection $jobs);

    /**
     * @return ArrayCollection
     */
    public function getJobs();

    /**
     * @param JobInterface $job
     *
     * @return bool
     */
    public function hasJob(JobInterface $job);

    /**
     * @param JobInterface $job
     *
     * @return $this
     */
    public function addJob(JobInterface $job);

    /**
     * @param JobInterface $job
     *
     * @return $this
     */
    public function removeJob(JobInterface $job);

    /**
     * @param Address $address
     *
     * @return $this
     */
    public function setAddress($address);

    /**
     * @return mixed
     */
    public function getAddress();

    /**
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * @param $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt);

    /**
     * @return \DateTime
     */
    public function getUpdatedAt();
}
