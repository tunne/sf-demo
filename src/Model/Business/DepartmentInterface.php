<?php
/**
 * DepartmentInterface.php.
 */

namespace Model\Business;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Interface DepartmentInterface.
 */
interface DepartmentInterface
{
    /**
     * @return integer
     */
    public function getId();

    /**
     * @param string $name
     *
     * @return Department
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param boolean $active
     *
     * @return Department
     */
    public function setActive($active);

    /**
     * @return boolean
     */
    public function getActive();

    /**
     * @param integer $position
     *
     * @return Department
     */
    public function setPosition($position);

    /**
     * @return integer
     */
    public function getPosition();

    /**
     * @param $pitch
     *
     * @return $this
     */
    public function setPitch($pitch);

    /**
     * @return string
     */
    public function getPitch();

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @param $description
     *
     * @return $this
     */
    public function setDescription($description);

    /**
     * @param ArrayCollection $jobs
     *
     * @return $this
     */
    public function setJobs(ArrayCollection $jobs);

    /**
     * @return ArrayCollection
     */
    public function getJobs();
    /**
     * @param JobInterface $job
     *
     * @return bool
     */
    public function hasJob(JobInterface $job);

    /**
     * @param JobInterface $job
     *
     * @return $this
     */
    public function addJob(JobInterface $job);

    /**
     * @param JobInterface $job
     *
     * @return $this
     */
    public function removeJob(JobInterface $job);

    /**
     * @param CompanyInterface $company
     *
     * @return $this
     */
    public function setCompany(CompanyInterface $company);

    /**
     * @return CompanyInterface
     */
    public function getCompany();

    /**
     * @param Address $address
     *
     * @return $this
     */
    public function setAddress($address);

    /**
     * @return mixed
     */
    public function getAddress();
}
