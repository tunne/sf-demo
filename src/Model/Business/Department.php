<?php
/**
 * Dapartment.php.
 */

namespace Model\Business;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Department.
 *
 * @ORM\Table(name="business_department")
 * @ORM\Entity(repositoryClass="Model\Business\Repository\DepartmentRepository")
 *
 * @Gedmo\Loggable
 *
 * @ExclusionPolicy("all")
 */
class Department implements DepartmentInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     * @SerializedName("id")
     * @Groups({"department_list", "department_show", "company_show"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     *
     * @Gedmo\Versioned
     *
     * @Expose
     * @SerializedName("name")
     * @Groups({"department_list", "department_show", "company_show"})
     *
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean")
     *
     * @Gedmo\Versioned
     *
     * @Expose
     * @SerializedName("active")
     * @Groups({"department_list", "department_show"})
     *
     * @Assert\NotBlank()
     */
    private $active;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer")
     *
     * @Gedmo\SortablePosition
     * @Gedmo\Versioned
     *
     * @Expose
     * @SerializedName("position")
     * @Groups({"department_list", "department_show"})
     *
     * @Assert\NotBlank()
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="pitch", type="string", length=255, nullable=true)
     *
     * @Gedmo\Versioned
     *
     * @Expose
     * @SerializedName("pitch")
     * @Groups({"department_list", "department_show"})
     *
     * @Assert\NotBlank()
     */
    private $pitch;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     *
     * @Gedmo\Versioned
     *
     * @Expose
     * @SerializedName("description")
     * @Groups({"department_show"})
     *
     * @Assert\NotBlank()
     */
    private $description;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Job", mappedBy="department")
     *
     * @Expose
     * @SerializedName("jobs")
     * @Groups({"department_show"})
     */
    private $jobs;

    /**
     * @var CompanyInterface
     *
     * @ORM\ManyToOne(targetEntity="Model\Business\Company", inversedBy="departments")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     *
     * @Gedmo\Versioned
     * @Gedmo\SortableGroup
     *
     * @Expose
     * @SerializedName("company")
     * @Groups({"department_show"})
     */
    private $company;

    /**
     * @Gedmo\Versioned
     * @ORM\OneToOne(targetEntity="Model\Geo\Address", mappedBy="department", cascade={"persist"}, orphanRemoval=false)
     *
     * @Expose
     * @SerializedName("address")
     * @Groups({"department_show", "company_show", "job_show"})
     */
    private $address;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     *
     * @Gedmo\Timestampable(on="create")
     *
     * @Expose
     * @SerializedName("created_at")
     * @Groups({"department_list", "department_show"})
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     *
     * @Gedmo\Timestampable(on="update")
     *
     * @Expose
     * @SerializedName("updated_at")
     * @Groups({"department_list", "department_show"})
     */
    private $updatedAt;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->position = 0;
        $this->active = false;
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->jobs = new ArrayCollection();
    }

    /**
     * clone.
     */
    public function __clone()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->jobs = new ArrayCollection();
    }

    /**
     * toString.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     *
     * @return Department
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param boolean $active
     *
     * @return Department
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param integer $position
     *
     * @return Department
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param $pitch
     *
     * @return $this
     */
    public function setPitch($pitch)
    {
        $this->pitch = $pitch;

        return $this;
    }

    /**
     * @return string
     */
    public function getPitch()
    {
        return $this->pitch;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @param ArrayCollection $jobs
     *
     * @return $this
     */
    public function setJobs(ArrayCollection $jobs)
    {
        $this->jobs = $jobs;

        if ($this->getCompany()) {
            foreach ($jobs as $job) {
                $this->getCompany()->addJob($job);
            }
        }

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getJobs()
    {
        return $this->jobs;
    }

    /**
     * @param JobInterface $job
     *
     * @return bool
     */
    public function hasJob(JobInterface $job)
    {
        if ($this->jobs->contains($job)) {
            return true;
        }

        return false;
    }

    /**
     * @param JobInterface $job
     *
     * @return $this
     */
    public function addJob(JobInterface $job)
    {
        if (!$this->hasJob($job)) {
            $this->jobs[] = $job;
            $job->setDepartment($this);
            if ($this->getCompany()) {
                $this->getCompany()->addJob($job);
            }
        }

        return $this;
    }

    /**
     * @param JobInterface $job
     *
     * @return $this
     */
    public function removeJob(JobInterface $job)
    {
        if ($this->hasJob($job)) {
            $this->jobs->removeElement($job);
        }

        return $this;
    }

    /**
     * @param CompanyInterface $company
     *
     * @return $this
     */
    public function setCompany(CompanyInterface $company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return CompanyInterface
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Address $address
     *
     * @return $this
     */
    public function setAddress($address)
    {
        if ($this->address) {
            $this->address->setDepartment(null);
        }
        if ($address) {
            $address->setDepartment($this);
        }
        $this->address = $address;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }
}
