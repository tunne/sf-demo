<?php
/**
 * Job.php.
 */

namespace Model\Business;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Model\Classification\TagInterface;
use Model\Classification\CategoryInterface;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;
use Symfony\Component\Validator\Constraints as Assert;
use Model\Geo\AddressInterface;

/**
 * Class Job.
 *
 * @ORM\Table(name="business_job")
 * @ORM\Entity(repositoryClass="Model\Business\Repository\JobRepository")
 *
 * @Gedmo\Loggable
 *
 * @ExclusionPolicy("all")
 */
class Job implements JobInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     * @SerializedName("id")
     * @Groups({"job_list", "job_show", "company_show", "department_show"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     *
     * @Gedmo\Versioned
     *
     * @Expose
     * @SerializedName("name")
     * @Groups({"job_list", "job_show", "company_show", "department_show"})
     *
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     *
     * @Gedmo\Versioned
     *
     * @Expose
     * @SerializedName("type")
     * @Groups({"job_list", "job_show", "company_show", "department_show"})
     *
     * @Assert\NotBlank()
     */
    private $type;

    /**
     * @var DepartmentInterface
     *
     * @ORM\ManyToOne(targetEntity="Department", inversedBy="jobs")
     * @ORM\JoinColumn(name="department_id", referencedColumnName="id")
     *
     * @Gedmo\Versioned
     *
     * @Expose
     * @SerializedName("department")
     * @Groups({"job_show"})
     */
    private $department;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="\Model\Classification\Category", inversedBy="jobs")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     *
     * @Gedmo\Versioned
     * @Expose
     * @SerializedName("category")
     * @Groups({"job_show"})
     */
    private $category;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="\Model\Classification\Tag", inversedBy="jobs")
     * @ORM\JoinTable(name="job_tag")
     *
     * @Expose
     * @SerializedName("tags")
     * @Groups({"job_show"})
     **/
    private $tags;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true, name="pitch")
     *
     * @Gedmo\Versioned
     *
     * @Expose
     * @SerializedName("intro")
     * @Groups({"job_show"})
     */
    private $pitch;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true, name="description")
     *
     * @Gedmo\Versioned
     *
     * @@Expose
     * @SerializedName("description")
     * @Groups({"job_show"})
     *
     * @Assert\NotBlank()
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true, name="requirements")
     *
     * @Gedmo\Versioned
     *
     * @Expose
     * @SerializedName("requirements")
     * @Groups({"job_show"})
     *
     * @Assert\NotBlank()
     */
    private $requirements;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true, name="offer")
     *
     * @Gedmo\Versioned
     *
     * @Expose
     * @SerializedName("offer")
     * @Groups({"job_show"})
     *
     * @Assert\NotBlank()
     */
    private $offer;

    /**
     * @var CompanyInterface
     *
     * @ORM\ManyToOne(targetEntity="Model\Business\Company", inversedBy="jobs")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     *
     * @Gedmo\Versioned
     * @Gedmo\SortableGroup
     *
     * @Expose
     * @SerializedName("company")
     * @Groups({"job_show"})
     */
    private $company;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", name="created_at")
     *
     * @Gedmo\Timestampable(on="create")
     *
     * @Expose
     * @SerializedName("created_at")
     * @Groups({"job_show"})
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", name="updated_at")
     *
     * @Gedmo\Timestampable(on="change")
     *
     * @Expose
     * @SerializedName("updated_at")
     * @Groups({"job_show"})
     */
    private $updatedAt;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     *
     * @return Job
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param DepartmentInterface $department
     *
     * @return $this
     */
    public function setDepartment(DepartmentInterface $department = null)
    {
        $this->department = $department;
        $department->addJob($this);
        return $this;
    }

    /**
     * @return DepartmentInterface
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param CategoryInterface $category
     *
     * @return $this
     */
    public function setCategory(CategoryInterface $category)
    {
        $this->category = $category;
        $category->addJob($this);

        return $this;
    }

    /**
     * @return DepartmentInterface
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return ArrayCollection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param TagInterface $tag
     *
     * @return bool
     */
    public function hasTag(TagInterface $tag)
    {
        if ($this->tags->contains($tag)) {
            return true;
        }

        return false;
    }

    /**
     * @param TagInterface $tag
     *
     * @return $this
     */
    public function addTag(TagInterface $tag)
    {
        if (!$this->hasTag($tag)) {
            $this->tags[] = $tag;
            $tag->addJob($this);
        }

        return $this;
    }

    /**
     * @param TagInterface $tag
     *
     * @return $this
     */
    public function removeTag(TagInterface $tag)
    {
        if ($this->hasTag($tag)) {
            $this->tags->remove($tag);
        }

        return $this;
    }

    /**
     * @param $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $pitch
     *
     * @return $this
     */
    public function setPitch($pitch)
    {
        $this->pitch = $pitch;

        return $this;
    }

    /**
     * @return string
     */
    public function getPitch()
    {
        return $this->pitch;
    }

    /**
     * @return CompanyInterface
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param CompanyInterface $company
     *
     * @return $this
     */
    public function setCompany(CompanyInterface $company)
    {
        $this->company = $company;
        $company->addJob($this);

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getRequirements()
    {
        return $this->requirements;
    }

    /**
     * @param $requirements
     *
     * @return $this
     */
    public function setRequirements($requirements)
    {
        $this->requirements = $requirements;

        return $this;
    }

    /**
     * @return string
     */
    public function getOffer()
    {
        return $this->offer;
    }

    /**
     * @param $offer
     *
     * @return $this
     */
    public function setOffer($offer)
    {
        $this->offer = $offer;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return AddressInterface|null
     *
     * @VirtualProperty
     * @SerializedName("address")
     * @Groups("job_show")
     */
    public function getAddress()
    {
        if ($this->getDepartment()) {
            if ($this->getDepartment()->getAddress()) {
                return $this->getDepartment()->getActive();
            }
        }

        if ($this->getCompany()) {
            if ($this->getCompany()->getAddress()) {
                return $this->getCompany()->getAddress();
            }
        }

        return;
    }

    /*
     * @return array
     */
    public static function getJobTypes()
    {
        return array(
            JobInterface::JOB_TYPE_FULLTIME => 'label.'.JobInterface::JOB_TYPE_FULLTIME,
            JobInterface::JOB_TYPE_PARTTIME => 'label.'.JobInterface::JOB_TYPE_PARTTIME,
        );
    }
}
