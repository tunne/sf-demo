<?php
/**
 * JobInterface.php.
 */

namespace Model\Business;

use Model\Classification\CategoryInterface;
use Model\Classification\TagInterface;

/**
 * Interface JobInterface.
 */
interface JobInterface
{
    const JOB_TYPE_FULLTIME     = 'job.type.fulltime';
    const JOB_TYPE_PARTTIME      = 'job.type.parttime';

    /**
     * @return integer
     */
    public function getId();

    /**
     * @param string $name
     *
     * @return Job
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param DepartmentInterface $department
     *
     * @return $this
     */
    public function setDepartment(DepartmentInterface $department = null);

    /**
     * @return DepartmentInterface
     */
    public function getDepartment();

    /**
     * @param CategoryInterface $category
     *
     * @return mixed
     */
    public function setCategory(CategoryInterface $category);

    /**
     * @return DepartmentInterface
     */
    public function getCategory();

    /**
     * @return ArrayCollection
     */
    public function getTags();

    /**
     * @param TagInterface $tag
     *
     * @return mixed
     */
    public function hasTag(TagInterface $tag);

    /**
     * @param TagInterface $tag
     *
     * @return mixed
     */
    public function addTag(TagInterface $tag);

    /**
     * @param TagInterface $tag
     *
     * @return mixed
     */
    public function removeTag(TagInterface $tag);

    /**
     * @param $description
     *
     * @return $this
     */
    public function setDescription($description);

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @param string $pitch
     *
     * @return $this
     */
    public function setPitch($pitch);

    /**
     * @return string
     */
    public function getPitch();
    /**
     * @return CompanyInterface
     */
    public function getCompany();

    /**
     * @param CompanyInterface $company
     *
     * @return $this
     */
    public function setCompany(CompanyInterface $company);

    /**
     * @return string
     */
    public function getType();

    /**
     * @param $type
     *
     * @return $this
     */
    public function setType($type);

    /**
     * @return string
     */
    public function getRequirements();

    /**
     * @param $requirements
     *
     * @return $this
     */
    public function setRequirements($requirements);

    /**
     * @return string
     */
    public function getOffer();

    /**
     * @param $offer
     *
     * @return $this
     */
    public function setOffer($offer);
    /**
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * @return \DateTime
     */
    public function getUpdatedAt();

    /**
     * @param $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt);

    /**
     * @return AddressInterface|null
     */
    public function getAddress();

    /*
     * @return array
     */
    public static function getJobTypes();
}
