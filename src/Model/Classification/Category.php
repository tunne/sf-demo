<?php
/**
 * Category.php.
 */

namespace Model\Classification;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use Model\Business\JobInterface;

/**
 * Class Category.
 *
 * @ORM\Entity(repositoryClass="Model\Classification\Repository\CategoryRepository")
 * @ORM\Table(name="classification_category")
 *
 * @Gedmo\Loggable
 *
 * @ExclusionPolicy("all")
 */
class Category implements CategoryInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     * @Groups({"category_show", "category_list", "job_list", "job_show"})
     */
    protected $id;

    /**
     * @ORM\Column(name="position", type="integer")
     *
     * @Gedmo\Versioned
     * @Gedmo\SortablePosition
     *
     * @Expose
     * @Groups({"category_show", "category_list", "job_list", "job_show"})
     */
    protected $position;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     *
     * @Gedmo\Versioned
     *
     * @Expose
     * @Groups({"category_show", "category_list", "job_list", "job_show"})
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     *
     * @Gedmo\Versioned
     * @Gedmo\Slug(fields={"name"})
     *
     * @Expose
     * @Groups({"category_show", "category_list", "job_list", "job_show"})
     */
    protected $slug;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean")
     *
     * @Gedmo\Versioned
     *
     * @Expose
     * @Groups({"category_show", "category_list", "job_list", "job_show"})
     */
    protected $active;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     *
     * @Gedmo\Versioned
     *
     * @Expose
     * @Groups({"category_show"})
     */
    protected $description;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     *
     * @Gedmo\Timestampable(on="create")
     *
     * @Expose
     * @Groups({"category_show"})
     */
    protected $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     *
     * @Gedmo\Versioned
     * @Gedmo\Timestampable(on="change")
     *
     * @Expose
     * @Groups({"category_show"})
     */
    protected $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="Model\Classification\Category", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     *
     * @Gedmo\Versioned
     *
     * @Expose
     * @Groups({})
     */
    protected $parent;

    /**
     * @ORM\OneToMany(targetEntity="Model\Classification\Category", mappedBy="parent")
     *
     * @Expose
     * @Groups({})
     */
    protected $children;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Model\Business\Job", mappedBy="category",  cascade={"persist"}))
     *
     * @Expose
     * @Groups({"category_show"})
     */
    protected $jobs;

    /**
     * constructor.
     */
    public function __construct()
    {
        $this->jobs = new ArrayCollection();
        $this->children = new ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->position = 0;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get id.
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param $position
     *
     * @return $this
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param $slug
     *
     * @return $this
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param $active
     *
     * @return $this
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Category $parent
     *
     * @return $this
     */
    public function setParent(CategoryInterface $parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param $children
     *
     * @return $this
     */
    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * @param $child
     *
     * @return bool
     */
    public function hasChild(CategoryInterface $child)
    {
        return $this->children->contains($child);
    }

    /**
     * @param Category $child
     *
     * @return $this
     */
    public function addChild(CategoryInterface $child)
    {
        if (!$this->hasChild($child)) {
            $this->children[] = $child;
        }

        return $this;
    }

    /**
     * @param $child
     *
     * @return $this
     */
    public function removeChild(CategoryInterface $child)
    {
        if ($this->hasChild($child)) {
            $this->children->removeElement($child);
        }

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getJobs()
    {
        return $this->jobs;
    }

    /**
     * @param $job
     *
     * @return bool
     */
    public function hasJob(JobInterface $job)
    {
        if ($this->jobs->contains($job)) {
            return true;
        }

        return false;
    }

    /**
     * @param $job
     *
     * @return $this
     */
    public function addJob(JobInterface $job)
    {
        if (!$this->hasJob($job)) {
            $this->jobs[] = $job;
        }

        return $this;
    }

    /**
     * @param $job
     *
     * @return $this
     */
    public function removeJob(JobInterface $job)
    {
        if ($this->hasJob($job)) {
            $this->jobs->remove($job);
        }

        return $this;
    }
}
