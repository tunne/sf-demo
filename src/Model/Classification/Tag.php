<?php
/**
 * Tag.php.
 */

namespace Model\Classification;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use Model\Business\JobInterface;

/**
 * Class Tag.
 *
 * @ORM\Entity(repositoryClass="Model\Classification\Repository\TagRepository")
 * @ORM\Table(name="classification_tag")
 *
 * @Gedmo\Loggable
 *
 * @ExclusionPolicy("all")
 */
class Tag implements TagInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Expose
     * @Groups({"tag_show", "tag_list", "job_show"})
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     *
     * @Gedmo\Versioned
     *
     * @Expose
     * @Groups({"tag_show", "tag_list", "job_show"})
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     *
     * @Gedmo\Versioned
     * @Gedmo\Slug(fields={"name"})
     *
     * @Expose
     * @Groups({"tag_show", "tag_list", "job_show"})
     */
    protected $slug;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     *
     * @Expose
     * @Groups({"tag_show", "tag_list", "job_show"})
     */
    protected $createdAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     *
     * @Gedmo\Versioned
     * @Gedmo\Timestampable(on="change")
     *
     * @Expose
     * @Groups({"tag_show", "tag_list", "job_show"})
     */
    protected $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="active", type="boolean")
     *
     * @Gedmo\Versioned
     *
     * @Expose
     * @Groups({"tag_show", "tag_list", "job_show"})
     */
    protected $active;

    /**
     * @ORM\ManyToMany(targetEntity="\Model\Business\Job", mappedBy="tags")
     * @Expose
     * @Groups({})
     **/
    protected $jobs;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->jobs = new ArrayCollection();
        $this->active = false;
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get id.
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param $slug
     *
     * @return $this
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param $active
     *
     * @return $this
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getJobs()
    {
        return $this->jobs;
    }

    /**
     * @param $job
     *
     * @return bool
     */
    public function hasJob(JobInterface $job)
    {
        if ($this->jobs->contains($job)) {
            return true;
        }

        return false;
    }

    /**
     * @param $job
     *
     * @return $this
     */
    public function addJob(JobInterface $job)
    {
        if (!$this->hasJob($job)) {
            $this->jobs[] = $job;
        }
        $job->addTag($this);

        return $this;
    }

    /**
     * @param $job
     *
     * @return $this
     */
    public function removeJob(JobInterface $job)
    {
        if ($this->hasJob($job)) {
            $this->jobs->remove($job);
        }

        return $this;
    }
}
