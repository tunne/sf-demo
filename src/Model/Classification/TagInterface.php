<?php
/**
 * TagInterface.php.
 */

namespace Model\Classification;

use Doctrine\Common\Collections\ArrayCollection;
use Model\Business\JobInterface;

/**
 * Interface TagInterface.
 */
interface TagInterface
{
    /**
     * Get id.
     *
     * @return int $id
     */
    public function getId();

    /**
     * @return string
     */
    public function getName();

    /**
     * @param $name
     *
     * @return $this
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getSlug();

    /**
     * @param $slug
     *
     * @return $this
     */
    public function setSlug($slug);

    /**
     * @return DateTime
     */
    public function getCreatedAt();

    /**
     * @return DateTime
     */
    public function getUpdatedAt();

    /**
     * @param $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt);

    /**
     * @return string
     */
    public function getActive();

    /**
     * @param $active
     *
     * @return $this
     */
    public function setActive($active);

    /**
     * @return ArrayCollection
     */
    public function getJobs();

    /**
     * @param $job
     *
     * @return bool
     */
    public function hasJob(JobInterface $job);

    /**
     * @param $job
     *
     * @return $this
     */
    public function addJob(JobInterface $job);

    /**
     * @param $job
     *
     * @return $this
     */
    public function removeJob(JobInterface $job);
}
