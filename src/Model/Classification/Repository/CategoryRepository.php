<?php
/**
 * CategoryRepository.php.
 */

namespace Model\Classification\Repository;

use Doctrine\ORM\EntityRepository;
use Model\Classification\Category;

/**
 * Class CategoryRepository.
 */
class CategoryRepository extends EntityRepository
{
    public function createNew()
    {
        return new Category();
    }

    /**
     * @return array
     */
    public function findCategoriesForIndex()
    {
        return $this->createQueryBuilder('c')
            ->join('c.jobs', 'cj')
            ->andWhere('c.active = true')
            ->getQuery()
            ->getResult()
            ;
    }

}
