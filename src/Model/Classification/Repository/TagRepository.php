<?php
/**
 * TagRepository.php.
 */

namespace Model\Classification\Repository;

use Doctrine\ORM\EntityRepository;
use Model\Classification\Tag;

/**
 * Class TagRepository.
 */
class TagRepository extends EntityRepository
{
    public function createNew()
    {
        return new Tag();
    }

    /**
     * @return array
     */
    public function findTagsForIndex()
    {
        return $this->createQueryBuilder('t')
            ->join('t.jobs', 'tj')
            ->andWhere('t.active = true')
            ->getQuery()
            ->getResult()
            ;
    }
}
