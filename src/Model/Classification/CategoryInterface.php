<?php
/**
 * CategoryInterface.php.
 */

namespace Model\Classification;

use Doctrine\Common\Collections\ArrayCollection;
use Model\Business\JobInterface;

/**
 * Interface CategoryInterface.
 */
interface CategoryInterface
{
    /**
     * @return int $id
     */
    public function getId();

    /**
     * @return int
     */
    public function getPosition();

    /**
     * @param $position
     *
     * @return $this
     */
    public function setPosition($position);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param $name
     *
     * @return $this
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getSlug();

    /**
     * @param $slug
     *
     * @return $this
     */
    public function setSlug($slug);

    /**
     * @return boolean
     */
    public function isActive();

    /**
     * @param bool $active
     *
     * @return $this
     */
    public function setActive($active);

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @param $description
     *
     * @return $this
     */
    public function setDescription($description);

    /**
     * @return mixed
     */
    public function getCreatedAt();

    /**
     * @return mixed
     */
    public function getUpdatedAt();

    /**
     * @param $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt);

    /**
     * @return mixed
     */
    public function getParent();

    /**
     * @param CategoryInterface $parent
     *
     * @return $this
     */
    public function setParent(CategoryInterface $parent);

    /**
     * @return mixed
     */
    public function getChildren();

    /**
     * @param $children
     *
     * @return $this
     */
    public function setChildren($children);

    /**
     * @param $child
     *
     * @return bool
     */
    public function hasChild(CategoryInterface $child);

    /**
     * @param CategoryInterface $child
     *
     * @return $this
     */
    public function addChild(CategoryInterface $child);

    /**
     * @param $child
     *
     * @return $this
     */
    public function removeChild(CategoryInterface $child);

    /**
     * @return ArrayCollection
     */
    public function getJobs();

    /**
     * @param $job
     *
     * @return bool
     */
    public function hasJob(JobInterface $job);

    /**
     * @param $job
     *
     * @return $this
     */
    public function addJob(JobInterface $job);

    /**
     * @param $job
     *
     * @return $this
     */
    public function removeJob(JobInterface $job);
}
