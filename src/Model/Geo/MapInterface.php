<?php
namespace Model\Geo;

/**
 * Interface MapInterface.
 */
interface MapInterface
{
    /**
     * @param boolean $autoZoom
     *
     * @return $this
     */
    public function setAutoZoom($autoZoom);

    /**
     * @return boolean
     */
    public function getAutoZoom();

    /**
     * @param float $centerLat
     *
     * @return $this
     */
    public function setCenterLat($centerLat);

    /**
     * @return float
     */
    public function getCenterLat();

    /**
     * @param float $centerLng
     *
     * @return $this
     */
    public function setCenterLng($centerLng);

    /**
     * @return float
     */
    public function getCenterLng();

    /**
     * @return int
     */
    public function getId();

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param int $zoomLevel
     *
     * @return $this
     */
    public function setZoomLevel($zoomLevel);

    /**
     * @return int
     */
    public function getZoomLevel();

    /**
     * @param string $style
     *
     * @return $this
     */
    public function setStyle($style);

    /**
     * @return string
     */
    public function getStyle();

    /**
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $markers
     */
    public function setMarkers($markers);

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getMarkers();

    /**
     * @param Marker $marker
     *
     * @return bool
     */
    public function hasMarker(Marker $marker);

    /**
     * @param Marker $marker
     *
     * @return $this
     */
    public function addMarker(Marker $marker);

    /**
     * @param $marker
     *
     * @return $this
     */
    public function removeMarker($marker);

    /**
     * @return \DateTime
     */
    public function getUpdatedAt();
}
