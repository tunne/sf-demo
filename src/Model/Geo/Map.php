<?php
/**
 * Map.php.
 */

namespace Model\Geo;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\SerializedName;

/**
 * Map.
 *
 * @Gedmo\Loggable
 * @ORM\Table(name="geo_map")
 * @ORM\Entity(repositoryClass="Model\Geo\Repository\MapRepository")
 *
 * @ExclusionPolicy("all")
 */
class Map implements MapInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Expose
     * @SerializedName("id")
     */
    private $id;

    /**
     * @var string
     * @Gedmo\Versioned
     * @ORM\Column(name="name", type="string", length=255)
     * @Expose
     * @SerializedName("name")
     */
    private $name;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Model\Geo\Marker", mappedBy="map", cascade={"persist"})
     * @Expose
     * @SerializedName("markers")
     */
    private $markers;

    /**
     * @var float
     * @Gedmo\Versioned
     * @ORM\Column(name="center_lat", type="float")
     * @Expose
     * @SerializedName("center_lat")
     */
    private $centerLat;

    /**
     * @var float
     * @Gedmo\Versioned
     * @ORM\Column(name="center_lng", type="float")
     * @Expose
     * @SerializedName("center_lng")
     */
    private $centerLng;

    /**
     * @var boolean
     * @Gedmo\Versioned
     * @ORM\Column(name="auto_zoom", type="boolean")
     * @Expose
     * @SerializedName("auto_zoom")
     */
    private $autoZoom;

    /**
     * @var integer
     * @Gedmo\Versioned
     * @ORM\Column(name="zoom_level", type="integer")
     * @Expose
     * @SerializedName("zoom_level")
     */
    private $zoomLevel;

    /**
     * @var string
     * @Gedmo\Versioned
     * @ORM\Column(name="style", type="string")
     * @Expose
     * @SerializedName("style")
     */
    private $style;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     * @Expose
     * @SerializedName("created_at")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="change")
     * @ORM\Column(name="updated_at", type="datetime")
     * @Expose
     * @SerializedName("updated_at")
     */
    private $updatedAt;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->markers = new ArrayCollection();
        $this->autoZoom = false;
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @param boolean $autoZoom
     *
     * @return $this
     */
    public function setAutoZoom($autoZoom)
    {
        $this->autoZoom = $autoZoom;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getAutoZoom()
    {
        return $this->autoZoom;
    }

    /**
     * @param float $centerLat
     *
     * @return $this
     */
    public function setCenterLat($centerLat)
    {
        $this->centerLat = $centerLat;

        return $this;
    }

    /**
     * @return float
     */
    public function getCenterLat()
    {
        return $this->centerLat;
    }

    /**
     * @param float $centerLng
     *
     * @return $this
     */
    public function setCenterLng($centerLng)
    {
        $this->centerLng = $centerLng;

        return $this;
    }

    /**
     * @return float
     */
    public function getCenterLng()
    {
        return $this->centerLng;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param int $zoomLevel
     *
     * @return $this
     */
    public function setZoomLevel($zoomLevel)
    {
        $this->zoomLevel = $zoomLevel;

        return $this;
    }

    /**
     * @return int
     */
    public function getZoomLevel()
    {
        return $this->zoomLevel;
    }

    /**
     * @param string $style
     *
     * @return $this
     */
    public function setStyle($style)
    {
        $this->style = $style;

        return $this;
    }

    /**
     * @return string
     */
    public function getStyle()
    {
        return $this->style;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $markers
     */
    public function setMarkers($markers)
    {
        $this->markers = $markers;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getMarkers()
    {
        return $this->markers;
    }

    /**
     * @param Marker $marker
     *
     * @return bool
     */
    public function hasMarker(Marker $marker)
    {
        if ($this->markers->contains($marker)) {
            return true;
        }

        return false;
    }

    /**
     * @param Marker $marker
     *
     * @return $this
     */
    public function addMarker(Marker $marker)
    {
        if (!$this->hasMarker($marker)) {
            $this->markers[] = $marker;
            $marker->setMap($this);
        }

        return $this;
    }

    /**
     * @param $marker
     *
     * @return $this
     */
    public function removeMarker($marker)
    {
        if ($this->hasMarker($marker)) {
            $this->markers->removeElement($marker);
        }

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
