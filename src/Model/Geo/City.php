<?php

namespace Model\Geo;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Groups;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class City.
 *
 * @ORM\Table(name="geo_city")
 * @ORM\Entity
 *
 * @ExclusionPolicy("all")
 */
class City
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @Expose
     * @SerializedName("id")
     * @Groups({"address_show", "address_list", "country_show", "province_show"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Expose
     * @SerializedName("name")
     * @Groups({"address_show", "address_list", "country_show", "province_show"})
     */
    private $name;

    /**
     * @var \Model\Geo\Country
     *
     * @ORM\ManyToOne(targetEntity="Model\Geo\Country", inversedBy="cities")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     *
     * @Expose
     * @SerializedName("country")
     * @Groups({})
     */
    private $country;

    /**
     * @var \Model\Geo\Country
     *
     * @ORM\ManyToOne(targetEntity="Model\Geo\Province", inversedBy="cities")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="province_id", referencedColumnName="id")
     * })
     *
     * @Expose
     * @SerializedName("province")
     * @Groups({})
     */
    private $province;

    /**
     * @ORM\OneToMany(targetEntity="Model\Geo\Address", mappedBy="city", cascade={"persist"})
     *
     * @Expose
     * @SerializedName("addresses")
     * @Groups({})
     **/
    private $addresses;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->addresses = new ArrayCollection();
    }

    /**
     * ToString.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set country.
     *
     * @param \Model\Geo\Country $country
     *
     * @return City
     */
    public function setCountry(\Model\Geo\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country.
     *
     * @return \Model\Geo\Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set province.
     *
     * @param \Model\Geo\Province $province
     *
     * @return City
     */
    public function setProvince(\Model\Geo\Province $province = null)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Get province.
     *
     * @return \Model\Geo\Province
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Add addresses.
     *
     * @param \Model\Geo\Address $addresses
     *
     * @return City
     */
    public function addAddress(AddressInterface $address)
    {
        if (!$this->hassAddress($address)) {
            $address->addCity($this);
            $this->addresses[] = $address;
        }

        return $this;
    }

    /**
     * Remove addresses.
     *
     * @param \Model\Geo\Address $addresses
     */
    public function removeAddress(\Model\Geo\Address $addresses)
    {
        $this->addresses->removeElement($addresses);
    }

    /**
     * Get addresses.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAddresses()
    {
        return $this->addresses;
    }
}
