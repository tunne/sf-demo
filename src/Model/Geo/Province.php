<?php
/**
 * Province.php.
 */

namespace Model\Geo;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Groups;

/**
 * Province.
 *
 * @ORM\Table(name="geo_province")
 * @ORM\Entity
 *
 * @ExclusionPolicy("all")
 */
class Province
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @Expose
     * @SerializedName("id")
     * @Groups({"address_show", "address_list", "country_show", "province_list", "province_show"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     *
     * @Expose
     * @SerializedName("name")
     * @Groups({"address_show", "address_list", "country_show", "province_list", "province_show"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="iso_name", type="string", nullable=true)
     *
     * @Expose
     * @SerializedName("iso_name")
     * @Groups({"address_show", "address_list", "country_show", "province_list", "province_show"})
     */
    private $isoName;

    /**
     * @var \Model\Geo\Country
     *
     * @ORM\ManyToOne(targetEntity="Model\Geo\Country", inversedBy="provinces")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     *
     * @Expose
     * @SerializedName("country")
     * @Groups({"province_show"})
     */
    private $country;

    /**
     * @ORM\OneToMany(targetEntity="Model\Geo\City", mappedBy="province", cascade={"persist"})
     *
     * @Expose
     * @SerializedName("cities")
     * @Groups({"province_show"})
     */
    private $cities;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Model\Geo\Address", mappedBy="province", cascade={"persist"})
     * @Expose
     * @SerializedName("addresses")
     * @Groups({})
     */
    private $addresses;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->cities = new ArrayCollection();
        $this->addresses = new ArrayCollection();
    }

    /**
     * ToString.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Province
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isoName.
     *
     * @param string $isoName
     *
     * @return Province
     */
    public function setIsoName($isoName)
    {
        $this->isoName = $isoName;

        return $this;
    }

    /**
     * Get isoName.
     *
     * @return string
     */
    public function getIsoName()
    {
        return $this->isoName;
    }

    /**
     * Get id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set country.
     *
     * @param \Model\Geo\Country $country
     *
     * @return Province
     */
    public function setCountry(\Model\Geo\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country.
     *
     * @return \Model\Geo\Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Add cities.
     *
     * @param \Model\Geo\City $cities
     *
     * @return Province
     */
    public function addCity(\Model\Geo\City $cities)
    {
        $this->cities[] = $cities;

        return $this;
    }

    /**
     * Remove cities.
     *
     * @param \Model\Geo\City $cities
     */
    public function removeCity(\Model\Geo\City $cities)
    {
        $this->cities->removeElement($cities);
    }

    /**
     * Get cities.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * @param AddressInterface $address
     *
     * @return bool
     */
    public function hasAddress(AddressInterface $address)
    {
        if ($this->addresses->contains($address)) {
            return true;
        }

        return false;
    }

    /**
     * @param AddressInterface $address
     *
     * @return $this
     */
    public function addAddress(AddressInterface $address)
    {
        if (!$this->hassAddress($address)) {
            $address->addProvince($this);
            $this->addresses[] = $address;
        }

        return $this;
    }

    /**
     * @param AddressInterface $addresses
     */
    public function removeAddress(AddressInterface $addresses)
    {
        $this->addresses->removeElement($addresses);
    }

    /**
     * Get addresses.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAddresses()
    {
        return $this->addresses;
    }
}
