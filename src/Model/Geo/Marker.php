<?php
/**
 * Marker.php.
 */

namespace Model\Geo;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Oh\GoogleMapFormTypeBundle\Validator\Constraints as OhAssert;

/**
 * Marker.
 *
 * @Gedmo\Loggable
 * @ORM\Table(name="geo_marker")
 * @ORM\Entity(repositoryClass="Model\Geo\Repository\MarkerRepository")
 *
 * @ExclusionPolicy("all")
 */
class Marker
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @Expose
     * @SerializedName("id")
     * @Groups({"address_show"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     *
     * @Gedmo\Versioned
     *
     * @Expose
     * @SerializedName("name")
     * @Groups({"address_show"})
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="lat", type="float")
     *
     * @Gedmo\Versioned
     *
     * @Expose
     * @SerializedName("lat")
     * @Groups({"address_show"})
     */
    private $lat;

    /**
     * @var float
     *
     * @ORM\Column(name="lng", type="float")
     *
     * @Gedmo\Versioned
     *
     * @Expose
     * @SerializedName("lng")
     * @Groups({"address_show"})
     */
    private $lng;

    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string", nullable=true)
     *
     * @Gedmo\Versioned
     *
     * @Expose
     * @SerializedName("icon")
     * @Groups({"address_show", "company_show", "department_show", "job_show"})
     */
    private $icon;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", nullable=true)
     *
     * @Gedmo\Versioned
     *
     * @Expose
     * @SerializedName("image")
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="animation", type="string", length=40, nullable=true)
     *
     * @Gedmo\Versioned
     *
     * @Expose
     * @SerializedName("animation")
     */
    private $animation;

    /**
     * @var array
     *
     * @ORM\Column(name="options", type="array")
     *
     * @Gedmo\Versioned
     *
     * @Expose
     * @SerializedName("options")
     */
    private $options;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToOne(targetEntity="Model\Geo\Address", mappedBy="marker", cascade={"persist"}, orphanRemoval=false)
     *
     * @Gedmo\Versioned
     *
     * @Expose
     * @SerializedName("address")
     */
    private $address;

    /**
     * @var \Model\Geo\Map
     *
     * @Gedmo\Versioned
     *
     * @ORM\ManyToOne(targetEntity="Model\Geo\Map", inversedBy="markers", cascade={"persist", "remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="map_id", referencedColumnName="id")
     * })
     *
     * @Expose
     * @SerializedName("map")
     */
    private $map;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     *
     * @Gedmo\Timestampable(on="create")
     *
     * @Expose
     * @SerializedName("created_at")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     *
     * @Gedmo\Timestampable(on="change")
     *
     * @Expose
     * @SerializedName("updated_at")
     */
    private $updatedAt;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->options = array();
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->lat = 0;
        $this->lng = 0;
    }

    /**
     * @param string $animation
     *
     * @return $this
     */
    public function setAnimation($animation)
    {
        $this->animation = $animation;

        return $this;
    }

    /**
     * @return string
     */
    public function getAnimation()
    {
        return $this->animation;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param string $icon
     *
     * @return $this
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $image
     *
     * @return $this
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param float $lat
     *
     * @return $this
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * @return float
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param float $lng
     *
     * @return $this
     */
    public function setLng($lng)
    {
        $this->lng = $lng;

        return $this;
    }

    /**
     * @return float
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * @param Map $map
     *
     * @return $this
     */
    public function setMap($map)
    {
        $this->map = $map;

        return $this;
    }

    /**
     * @return \Model\Geo\Map
     */
    public function getMap()
    {
        return $this->map;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $options
     *
     * @return $this
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param AddressInterface $address
     *
     * @return $this
     */
    public function setAddress(AddressInterface $address)
    {
        $this->address = $address;

        return $this;
    }

    public function setLatLng($latlng)
    {
        $this->setLat($latlng['lat']);
        $this->setLng($latlng['lng']);

        return $this;
    }

    /**
     * @Assert\NotBlank()
     * @OhAssert\LatLng()
     *
     * @VirtualProperty
     * @SerializedName("lat_lng")
     * @Groups({"address_show", "company_show", "department_show", "job_show"})
     */
    public function getLatLng()
    {
        return array('lat' => $this->getLat(),'lng' => $this->getLng());
    }
}
