<?php
/**
 * AddressRepository.php.
 */

namespace Model\Geo\Repository;

use Doctrine\ORM\EntityRepository;
use Model\Geo\Address;
use Model\Geo\AddressInterface;

/**
 * Class AddressRepository.
 */
class AddressRepository extends EntityRepository
{
    /**
     * @return AddressInterface
     */
    public function createNew()
    {
        return new Address();
    }
}
