<?php
/**
 * Address.php.
 */

namespace Model\Geo;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;
use Symfony\Component\Validator\Constraints as Assert;
use Model\Business\CompanyInterface;
use Model\Business\DepartmentInterface;
use Model\Auth\User;

/**
 * Address.
 *
 * @ORM\Table(name="geo_address")
 * @ORM\Entity(repositoryClass="Model\Geo\Repository\AddressRepository")
 *
 * @ExclusionPolicy("all")
 */
class Address implements AddressInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @Expose
     * @SerializedName("id")
     * @Groups({"address_show", "address_list", "company_list", "company_show", "department_list", "department_show", "job_show"})
     */
    private $id;

    /**
     * @var CompanyInterface
     *
     * @ORM\OneToOne(targetEntity="Model\Business\Company", inversedBy="address", cascade={"persist"})
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id", onDelete="SET NULL")
     *
     * @Expose
     * @SerializedName("company")
     * @Groups({"address_show"})
     */
    private $company;

    /**
     * @var DepartmentInterface
     *
     * @ORM\OneToOne(targetEntity="Model\Business\Department", inversedBy="address", cascade={"persist"})
     * @ORM\JoinColumn(name="department_id", referencedColumnName="id", onDelete="SET NULL")
     *
     * @Expose
     * @SerializedName("department")
     * @Groups({"address_show"})
     */
    private $department;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string")
     * @Expose
     * @SerializedName("street")
     *
     * @Groups({"address_show", "address_list", "company_list", "company_show", "department_list", "department_show", "job_show"})
     *
     * @Assert\NotBlank()
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="nr", type="string", length=40)
     * @Expose
     * @SerializedName("nr")
     *
     * @Groups({"address_show", "address_list", "company_list", "company_show", "department_list", "department_show", "job_show"})
     *
     * @Assert\NotBlank()
     */
    private $nr;

    /**
     * @var User
     *
     * @ORM\ManyToMany(targetEntity="Model\Auth\User", inversedBy="addresses", cascade={"persist"})
     * @ORM\JoinTable(name="user_address")
     *
     * @Expose
     * @SerializedName("users")
     * @Groups({"address_show"})
     **/
    private $users;

    /**
     * @var \Model\Geo\City
     *
     * @ORM\ManyToOne(targetEntity="Model\Geo\City", inversedBy="addresses")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     * })
     *
     * @Expose
     * @SerializedName("city")
     *
     * @Groups({"address_show", "address_list"})
     *
     * @Assert\NotBlank()
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="postcode", type="string")
     *
     * @Expose
     * @SerializedName("postcode")
     *
     * @Groups({"address_show", "address_list", "company_show", "department_show", "job_show"})
     *
     * @Assert\NotBlank()
     */
    private $postcode;

    /**
     * @var \Model\Geo\Country
     *
     * @ORM\ManyToOne(targetEntity="Model\Geo\Country", inversedBy="addresses")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     * @Expose
     * @SerializedName("country")
     *
     * @Groups({"address_show", "address_list"})
     *
     * @Assert\NotBlank()
     */
    private $country;

    /**
     * @var \Model\Geo\Province
     *
     * @ORM\ManyToOne(targetEntity="Model\Geo\Province", inversedBy="addresses")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="province_id", referencedColumnName="id")
     * })
     * @Expose
     * @SerializedName("province")
     *
     * @Groups({"address_show", "address_list"})
     * @Assert\NotBlank()
     */
    private $province;

    /**
     * @ORM\OneToOne(targetEntity="Model\Geo\Marker", inversedBy="address", cascade={"persist"})
     * @ORM\JoinColumn(name="marker_id", referencedColumnName="id", onDelete="SET NULL")
     * @Expose
     * @SerializedName("marker")
     *
     * @Groups({"address_show", "company_show", "department_show", "job_show"})
     */
    private $marker;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     * @Expose
     * @SerializedName("created_at")
     *
     * @Groups({"address_show"})
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="update")
     * @Expose
     * @SerializedName("updated_at")
     *
     * @Groups({"address_show"})
     */
    private $updatedAt;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    public function __toString()
    {
        return $this->getStreet().' '.$this->getNr();
    }

    /**
     * @param CompanyInterface $company
     *
     * @return $this
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Department $department
     *
     * @return $this
     */
    public function setDepartment($department)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set street.
     *
     * @param string $street
     *
     * @return Address
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street.
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param $nr
     *
     * @return $this
     */
    public function setNr($nr)
    {
        $this->nr = $nr;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNr()
    {
        return $this->nr;
    }

    /**
     * Set city.
     *
     * @param string $city
     *
     * @return Address
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city.
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set postcode.
     *
     * @param string $postcode
     *
     * @return Address
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode.
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Address
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return Address
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set country.
     *
     * @param \Model\Geo\Country $country
     *
     * @return Address
     */
    public function setCountry(Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country.
     *
     * @return \Model\Geo\Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set province.
     *
     * @param \Model\Geo\Province $province
     *
     * @return Address
     */
    public function setProvince(Province $province = null)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Get province.
     *
     * @return \Model\Geo\Province
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Add users.
     *
     * @param \Model\Auth\User $users
     *
     * @return Address
     */
    public function addUser(User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users.
     *
     * @param \Model\Auth\User $users
     */
    public function removeUser(User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @return mixed
     */
    public function getMarker()
    {
        return $this->marker;
    }

    /**
     * @param $marker
     *
     * @return $this
     */
    public function setMarker(Marker $marker)
    {
        $this->marker = $marker;

        return $this;
    }

    /**
     * @return null|string
     *
     * @VirtualProperty
     * @SerializedName("country_name")
     * @Groups({"company_list", "company_show", "department_list", "department_show", "job_show"})
     */
    public function getCountryName()
    {
        if ($this->getCountry()) {
            return $this->getCountry()->getName();
        }

        return;
    }

    /**
     * @return null|string
     *
     * @VirtualProperty
     * @SerializedName("province_name")
     * @Groups({"company_list", "company_show", "department_list", "department_show", "job_show"})
     */
    public function getProvinceName()
    {
        if ($this->getProvince()) {
            return $this->getProvince()->getName();
        }

        return;
    }

    /**
     * @return null|string
     *
     * @VirtualProperty
     * @SerializedName("city_name")
     * @Groups({"company_list", "company_show", "department_list", "department_show", "job_show"})
     */
    public function getCityName()
    {
        if ($this->getCity()) {
            return $this->getCity()->getName();
        }

        return;
    }
}
