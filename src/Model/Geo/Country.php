<?php
/**
 * Country.php.
 */

namespace Model\Geo;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Groups;

/**
 * Country.
 *
 * @ORM\Table(name="geo_country")
 * @ORM\Entity
 * @ExclusionPolicy("all")
 */
class Country
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @Expose
     * @SerializedName("id")
     * @Groups({"address_show", "address_list", "country_list", "country_show", "province_show"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     *
     * @Expose
     * @SerializedName("name")
     * @Groups({"address_show", "address_list", "country_list", "country_show", "province_show"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="iso_name", type="string")
     *
     * @Expose
     * @SerializedName("iso_name")
     * @Groups({"address_show", "address_list", "country_list", "country_show", "province_show"})
     */
    private $isoName;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Model\Geo\Province", mappedBy="country", cascade={"persist"})
     *
     * @Expose
     * @SerializedName("provinces")
     * @Groups({"country_show"})
     */
    private $provinces;

    /**
     * @ORM\OneToMany(targetEntity="Model\Geo\City", mappedBy="country", cascade={"persist"})
     *
     * @Expose
     * @SerializedName("cities")
     *
     * @Groups({"country_show"})
     */
    private $cities;

    /**
     * @ORM\OneToMany(targetEntity="Model\Geo\Address", mappedBy="country", cascade={"persist"})
     *
     * @Expose
     * @serializedName("addresses")
     * @Groups({"country_show"})
     */
    private $addresses;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->provinces = new ArrayCollection();
        $this->cities = new ArrayCollection();
        $this->addresses = new ArrayCollection();
    }

    /**
     * ToString.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Country
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isoName.
     *
     * @param string $isoName
     *
     * @return Country
     */
    public function setIsoName($isoName)
    {
        $this->isoName = $isoName;

        return $this;
    }

    /**
     * Get isoName.
     *
     * @return string
     */
    public function getIsoName()
    {
        return $this->isoName;
    }

    /**
     * Get id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add provinces.
     *
     * @param \Model\Geo\Country $provinces
     *
     * @return Country
     */
    public function addProvince(Country $provinces)
    {
        $this->provinces[] = $provinces;

        return $this;
    }

    /**
     * Remove provinces.
     *
     * @param \Model\Geo\Country $provinces
     */
    public function removeProvince(Country $provinces)
    {
        $this->provinces->removeElement($provinces);
    }

    /**
     * Get provinces.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProvinces()
    {
        return $this->provinces;
    }

    /**
     * Add cities.
     *
     * @param \Model\Geo\City $cities
     *
     * @return Country
     */
    public function addCity(City $cities)
    {
        $this->cities[] = $cities;

        return $this;
    }

    /**
     * Remove cities.
     *
     * @param \Model\Geo\City $cities
     */
    public function removeCity(City $cities)
    {
        $this->cities->removeElement($cities);
    }

    /**
     * Get cities.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * @param AddressInterface $address
     *
     * @return bool
     */
    public function hasAddress(AddressInterface $address)
    {
        if ($this->addresses->contains($address)) {
            return true;
        }

        return false;
    }

    /**
     * @param AddressInterface $address
     *
     * @return $this
     */
    public function addAddress(AddressInterface $address)
    {
        if (!$this->hassAddress($address)) {
            $address->addCountry($this);
            $this->addresses[] = $address;
        }

        return $this;
    }

    /**
     * @param AddressInterface $addresses
     */
    public function removeAddress(AddressInterface $addresses)
    {
        $this->addresses->removeElement($addresses);
    }

    /**
     * Get addresses.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAddresses()
    {
        return $this->addresses;
    }
}
