<?php
/**
 * Message.php.
 */

namespace Model\Notification;

use Doctrine\ORM\Mapping as ORM;
use Sonata\NotificationBundle\Entity\BaseMessage as BaseMessage;

/**
 * Class Message.
 *
 * @ORM\Entity
 * @ORM\Table(name="app_notification_message")
 */
class Message extends BaseMessage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Get id.
     *
     * @return int $id
     */
    public function getId()
    {
        return $this->id;
    }
}
