<?php

namespace Model\Auth;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Model\Geo\AddressInterface;
use Model\Business\CompanyInterface;
use Sonata\UserBundle\Entity\BaseUser;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

/**
 * User.
 *
 * @Gedmo\Loggable
 * @ORM\Table(name="auth_user")
 * @ORM\Entity
 *
 * @ExclusionPolicy("all")
 */
class User extends BaseUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @Expose
     * @Groups({"user_list", "user_show", "company_show"})
     */
    protected $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Model\Auth\Group", cascade={"all"})
     * @ORM\JoinTable(name="app_user_group",
     *   joinColumns={
     *     @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="group_id", referencedColumnName="id", onDelete="CASCADE")
     *   }
     * )
     *
     * @Expose
     * @Groups({"user_show"})
     */
    protected $groups;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Model\Geo\Address", mappedBy="users", cascade={"persist"})
     *
     * @Expose
     * @Groups({"user_show"})
     **/
    protected $addresses;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Model\Business\Company", mappedBy="owner", cascade={"persist"})
     *
     * @Expose
     * @Groups({"user_show"})
     */
    protected $companies;

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent:: __construct();
    }

    /**
     * Get id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param AddressInterface $address
     *
     * @return bool
     */
    public function hasAddress(AddressInterface $address)
    {
        if ($this->addresses->contains($address)) {
            return true;
        }

        return false;
    }

    /**
     * @param AddressInterface $address
     *
     * @return $this
     */
    public function addAddress(AddressInterface $address)
    {
        if (!$this->hassAddress($address)) {
            $address->addUser($this);
            $this->addresses[] = $address;
        }

        return $this;
    }

    /**
     * @param AddressInterface $addresses
     */
    public function removeAddress(AddressInterface $addresses)
    {
        $this->addresses->removeElement($addresses);
    }

    /**
     * Get addresses.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAddresses()
    {
        return $this->addresses;
    }

    /**
     * @param CompanyInterface $company
     *
     * @return bool
     */
    public function hasCompany(CompanyInterface $company)
    {
        if ($this->companies->contains($company)) {
            return true;
        }

        return false;
    }

    /**
     * @param CompanyInterface $company
     *
     * @return $this
     */
    public function addCompany(CompanyInterface $company)
    {
        if (!$this->hasCompany($company)) {
            $company->setOwner($this);
            $this->companies[] = $company;
        }

        return $this;
    }

    /**
     * @param CompanyInterface $company
     *
     * @return $this
     */
    public function removeCompany(CompanyInterface $company)
    {
        if ($this->hasCompany($company)) {
            $this->companies->removeElement($company);
        }

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getCompanies()
    {
        return $this->companies;
    }
}
